<div class="center-title">![Readme Screenshot](./screenshots/Readme.png)</div>

<div class="center-title">

# Restaurant App

</div>

<div class="container">

<div class="topleft-column">![Project Screenshot 1](./screenshots/image1.png)</div>

<div class="topcenter-column">

# Project Description

Introducing cutting-edge Android application designed exclusively for restaurant and cafe owners, aimed at providing customers with seamless browsing and ordering experiences. With it's user-friendly interface, customers can easily browse through an extensive menu options, place orders, and make reservations with just a few taps. This application provides customers with real-time updates on their orders and reservations, ensuring a hassle-free dining experience. The application before you is designed to streamline the ordering process and improve customer satisfaction, ultimately leading to increased revenue and customer loyalty. Experience the future of dining with this state-of-the-art Android application.


</div>

<div class="topright-column">

# My Tech Stack:

*   Kotlin, Java
*   AndroidX
*   Material Design
*   Jetpack Compose
*   Coroutines & Flow
*   Android Architecture Components
*   Dagger Hilt
*   Gradle
*   GitLab
*   JUnit
*   Jupiter
*   Mockito
*   MockK
*   Espresso
*   Web Service Integration (REST, JSON, XML, GSON)
*   Firebase
*   Firestore
*   Room DB
*   REST
*   RESTful APIs
*   Retrofit2
*   GraphQL (Apollo)
*   Glide
*   Glide for Jetpack Compose
*   Coil for Jetpack Compose
*   Google Pay
*   Google Location
*   MVI
*   MVVM
*   MVC
*   Service Locator
*   Clean Architecture
*   Dependency Injection
*   CI/CD
*   Clean Code Principles
*   Agile Software Development Practices
*   Test Driven Development

</div>

</div>

<div class="container">

<div class="left-column">

# Target Customer Persona

Target Customer Persona
The average user of our restaurant or cafe Android application is a tech-savvy individual who values convenience and efficiency in their dining experiences. They are likely to be frequent diners who appreciate the convenience of being able to browse menus, place orders, and make reservations directly from their mobile device. They are comfortable using technology to simplify their lives and prefer to avoid the traditional methods of calling in orders or making reservations. They value real-time updates on their orders and appreciate being able to track their food's progress from the kitchen to their table. Overall, an average user of this app is someone who wants to make the most out of their dining experience, while minimizing wait times and maximizing convenience.


</div>

<div class="right-column">![Project Screenshot 2](./screenshots/image2.png)</div>

</div>

<div class="container">

<div class="left-column">![Project Screenshot 3](./screenshots/image3.png)</div>

<div class="right-column">

# Problem Statement
This application has been designed to solve following problems of their target users.
	1. Convenient browsing of menus: Users can easily browse menus on their mobile devices, eliminating the need to physically handle menu cards or ask for recommendations from waitstaff.
	2. Streamlined ordering process: Users can place orders directly through the application, reducing the wait time for their food and improving order accuracy.
	3. Real-time updates on orders: Users receive notifications on their mobile device regarding the status of their order, allowing them to track its progress from the kitchen to their table.
	4. Hassle-free reservations: Users can make reservations directly through the application, eliminating the need to call the restaurant or physically visit it.
	5. Increased efficiency: The application helps restaurants and cafes operate more efficiently by reducing the time staff members spend taking orders and making reservations over the phone.
	6. Enhanced customer satisfaction: The application provides users with a seamless dining experience, ultimately leading to improved customer satisfaction and loyalty.


</div>

</div>

<div class="container">

<div class="right-column">

# Functionalities

	1. Menu browsing: The application allows users to browse the menu of the restaurant or cafe on their mobile devices, with options to filter by categories, allergens or dietary requirements.
	2. Item selection: Users can select items from the menu, customize them according to their preferences, and add them to their order.
	3. Order placement: Users can place orders directly through the application, with options to pay online or in person upon arrival.
	4. Reservation scheduling: Users can make reservations through the application by specifying the date, time, and number of guests.
	5. Order tracking: The application provides real-time updates on the status of a user's order, including estimated time of arrival and progress of the order.
	6. Order history: Users can view their order history and re-order previous meals or favorite items with ease.
	7. Rating and review: Users can rate and review their dining experience, providing valuable feedback to the restaurant or cafe.
	8. Loyalty program: The application can offer loyalty rewards to frequent users, such as discounts, free meals or special offers.
	9. Notifications: Users can receive notifications for order updates, reservation confirmations, and promotions.
	10. Contact information: The application provides contact information for the restaurant or cafe, including address, phone number, email and social media handles.
	11. Table reservation management: The application allows restaurant or cafe owners to manage their table reservations, including seating arrangements and availability.
	12. Menu management: The application provides restaurant or cafe owners with the ability to manage their menu, including adding or removing items, updating prices and descriptions, and setting allergen and dietary information.


</div>

<div class="left-column">![Project Screenshot 4](./screenshots/image4.png)</div>

</div>

<div class="container">

<div class="right-column">

# Application's Screens

	1. Splash screen: This screen displays the application's logo and provides a loading indicator while the application is being initialized.
	2. Home screen: This screen serves as the main landing page of the application, providing options to browse menus, place orders, and make reservations.
	3. Menu screen: This screen displays the restaurant or cafe's menu, with options to filter by categories, allergens, or dietary requirements.
	4. Item details screen: This screen displays the details of a selected menu item, including description, price, and customizable options.
	5. Cart screen: This screen displays the current order, including the selected items and their quantities, with options to modify or remove items.
	6. Checkout screen: This screen displays the total cost of the order and provides options to pay online or in person upon arrival.
	7. Reservation screen: This screen provides options to make a reservation by specifying the date, time, and number of guests.
	8. Reservation confirmation screen: This screen confirms the reservation and provides a reference number or confirmation code.
	9. Order status screen: This screen displays real-time updates on the status of a user's order, including estimated time of arrival and progress of the order.
	10. Order history screen: This screen displays the user's order history, including previous orders and favorite items.
	11. Rating and review screen: This screen allows users to rate and review their dining experience, providing valuable feedback to the restaurant or cafe.
	12. Loyalty program screen: This screen displays the user's loyalty rewards, such as discounts, free meals, or special offers.
	13. Notifications screen: This screen displays notifications for order updates, reservation confirmations, and promotions.
	14. Profile screen: This screen displays the user's profile information, including name, contact information, and loyalty program status.
	15. Settings screen: This screen allows users to customize their application settings, such as language preferences and notification settings
	16. Contact screen: This screen displays the contact information for the restaurant or cafe, including address, phone number, email, and social media handles.
	17. Admin screen: This screen is only accessible to restaurant or cafe owners and allows them to manage their table reservations, menu items, and order history.


</div>

</div>

<div class="container">

<div class="right-column">

# Disclaimer

In order to provide their clients with protection of trade secrets, protection of user data, legal protection and competitive advantage all commissions of projects of this Developer are by default accompanied by an appropriate Non Disclosure Agreement (NDA). For the same reason all presented: project description, problem statement, functionalities and screen descriptions are for presentation purposes only and may or may not represent an entirety of project.

Due to NDA and obvious copyright and security issues, this GitHub project does not contain entire code of described application.

All included code snippets are for presentation purposes only and should not be used for commercial purposes as they are intellectual property of this application's Developer. In compliance with an appropriate NDA, all presented screenshots of working application have been made after removing any and all graphics that may be considered third party's intellectual property. As such they do not represent a final end product.

The developer does not claim any ownership or affiliation with any of the third-party libraries, technologies, or services used in the project.

While every effort has been made to ensure the accuracy, completeness, and reliability of the presented code, the developer cannot be held responsible for any errors, omissions, or damages that may arise from it's use.

In compliance with an appropriate NDA and in order to provide their clients with all presented: description, target customer persona, problem statement, functionalities, screen descriptions and other items are for presentation purposes only and do not consist an entirety of commisioned project.

Protection of trade secrets, protection of user data, legal protection and protection of competitive advantage of a Client is of utmost importance to the Developer.

</div>

</div>

<div class="container">

<div class="right-column">

# License

Any use of this code is prohibited except of for recruitment purposes only of the Developer who wrote it.

</div>

</div>