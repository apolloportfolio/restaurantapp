package com.aps.catemplateapp.core.util

import androidx.test.espresso.IdlingRegistry
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.util.EspressoIdlingResource
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class EspressoIdlingResourceRule: TestWatcher() {
    private val CLASS_NAME = "EspressoIdlingResourceRule"

    private val idlingResource = EspressoIdlingResource.countingIdlingResource

    override fun finished(description: Description?) {
        ALog.d(CLASS_NAME, ".decrement(): FINISHED")
        IdlingRegistry.getInstance().unregister(idlingResource)
        super.finished(description)
    }

    override fun starting(description: Description?) {
        ALog.d(CLASS_NAME, ".decrement(): STARTING")
        IdlingRegistry.getInstance().register(idlingResource)
        super.starting(description)
    }
}