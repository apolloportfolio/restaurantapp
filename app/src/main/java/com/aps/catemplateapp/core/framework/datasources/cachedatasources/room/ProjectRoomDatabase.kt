package com.aps.catemplateapp.core.framework.datasources.cachedatasources.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.*
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.DishDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.OrderDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.ReservationDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.DishCacheEntity
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.OrderCacheEntity
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.ReservationCacheEntity

@Database(entities = [
    UserCacheEntity::class,
    DishCacheEntity::class,
    OrderCacheEntity::class,
    ReservationCacheEntity::class,
], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class ProjectRoomDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao

    // RestaurantApp
    abstract fun dishDao() : DishDao

    abstract fun orderDao() : OrderDao

    abstract fun reservationDao() : ReservationDao

    companion object {
        const val DATABASE_NAME: String = "APSDB"
    }
}