package com.aps.catemplateapp.restaurantapp.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReservationFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        picture1URI: String?,
        description : String?,

        ownerID: UserUniqueID?,

        date: String?,
        time: String?,
        numberOfGuests: String?,
    ): Reservation {
        return Reservation(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            picture1URI,
            description,

            ownerID,

            date,
            time,
            numberOfGuests,
        )
    }

    fun createEntitiesList(numEntities: Int): List<Reservation> {
        val list: ArrayList<Reservation> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): Reservation {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<Reservation> {
            return arrayListOf(
                Reservation(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-14",
                    time = "10:00:00 AM",
                    numberOfGuests = "1",
                ),
                Reservation(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "11:00:00 AM",
                    numberOfGuests = "1",
                ),
                Reservation(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-1",
                    time = "12:00:00 AM",
                    numberOfGuests = "2",
                ),
                Reservation(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "13:00:00 AM",
                    numberOfGuests = "3",
                ),
                Reservation(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "09:00:00 AM",
                    numberOfGuests = "6",
                ),
                Reservation(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "11:00:00 AM",
                    numberOfGuests = "1",
                ),
                Reservation(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "11:00:00 AM",
                    numberOfGuests = "1",
                ),
                Reservation(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "11:00:00 AM",
                    numberOfGuests = "1",
                ),
                Reservation(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "11:00:00 AM",
                    numberOfGuests = "1",
                ),
                Reservation(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                    date = "2019-04-16",
                    time = "11:00:00 AM",
                    numberOfGuests = "1",
                )
            )
        }
    }
}