package com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.mappers.OrderFirestoreMapper
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.model.OrderFirestoreEntity

interface OrderFirestoreService: BasicFirestoreService<
        Order,
        OrderFirestoreEntity,
        OrderFirestoreMapper
        > {
    suspend fun getUsersEntities2(userId: UserUniqueID): List<Order>?

    suspend fun searchEntities(
        dishName: String,
    ) : List<Order>?
}