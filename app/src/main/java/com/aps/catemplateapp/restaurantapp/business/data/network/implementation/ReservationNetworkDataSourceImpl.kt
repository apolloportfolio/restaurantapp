package com.aps.catemplateapp.restaurantapp.business.data.network.implementation

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.ReservationNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs.ReservationFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReservationNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: ReservationFirestoreService
): ReservationNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Reservation): Reservation? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: Reservation) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<Reservation>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: Reservation) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<Reservation> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: Reservation): Reservation? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<Reservation> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<Reservation>): List<Reservation>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): Reservation? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<Reservation>? {
        return firestoreService.getUsersEntities3(userId)
    }
}