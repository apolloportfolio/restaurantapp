package com.aps.catemplateapp.restaurantapp.business.data.network.implementation

import android.net.Uri
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.DishNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.business.interactors.implementation.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs.DishFirestoreService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DishNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: DishFirestoreService
): DishNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Dish): Dish? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: Dish) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<Dish>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: Dish) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<Dish> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: Dish): Dish? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<Dish> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<Dish>): List<Dish>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): Dish? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<Dish>? {
        if(LOG_ME) ALog.d(TAG, ".searchEntities(): searchParameters:\n$searchParameters")
        return firestoreService.searchEntities(searchParameters)
    }

    override suspend fun getUsersEntites1(userID: UserUniqueID) : List<Dish>? {
        return firestoreService.getUsersEntities1(userID)
    }

    override suspend fun uploadEntity1PhotoToFirestore(
        entity : Dish,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String? {
        return firestoreService.uploadEntity1PhotoToFirestore(
            entity,
            entitysPhotoUri,
            entitysPhotoNumber
        )
    }

    companion object {
        const val TAG = "Entity1NetworkDataSourceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity1"
        const val DELETES_COLLECTION_NAME = "entity1_d"
    }
}
