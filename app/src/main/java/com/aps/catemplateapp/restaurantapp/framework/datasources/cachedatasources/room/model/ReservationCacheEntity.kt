package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID

@Entity(tableName = "reservation")
data class ReservationCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "ownerID")
    var ownerID: UserUniqueID?,


    @ColumnInfo(name = "date")
    var date: String?,

    @ColumnInfo(name = "time")
    var time: String?,

    @ColumnInfo(name = "numberOfGuests")
    var numberOfGuests: String?,
    ) {
}