package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.OrderCacheEntity
import java.util.*


@Dao
interface OrderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : OrderCacheEntity) : Long

    @Query("SELECT * FROM orderentity")
    suspend fun get() : List<OrderCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<OrderCacheEntity>): LongArray

    @Query("SELECT * FROM orderentity WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): OrderCacheEntity?

    @Query("DELETE FROM orderentity WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM orderentity")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM orderentity")
    suspend fun getAllEntities(): List<OrderCacheEntity>

    @Query("""
        UPDATE orderentity 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        description = :description,
        ownerId = :ownerID,
        orderedDishesIds = :orderedDishesIds
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        orderedDishesIds: String?,
    ): Int

    @Query("DELETE FROM orderentity WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM orderentity")
    suspend fun searchEntities(): List<OrderCacheEntity>
    
    @Query("SELECT COUNT(*) FROM orderentity")
    suspend fun getNumEntities(): Int
}