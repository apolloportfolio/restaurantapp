package com.aps.catemplateapp.restaurantapp.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.restaurantapp.RestaurantAppViewModelFactory
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.DishFactory
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.restaurantapp.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Named
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object RestaurantAppViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    @Named("Feature2ViewModelFactory")
    fun provideFeature2ViewModelFactory(
        homeScreenInteractors: HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        dishFactory: DishFactory,
        editor: SharedPreferences.Editor,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return RestaurantAppViewModelFactory(
            homeScreenInteractors = homeScreenInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            dishFactory = dishFactory,
            editor = editor,
        )
    }
}