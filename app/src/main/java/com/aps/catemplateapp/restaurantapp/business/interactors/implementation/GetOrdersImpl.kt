package com.aps.catemplateapp.restaurantapp.business.interactors.implementation

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.OrderCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.OrderNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.OrderFactory
import com.aps.catemplateapp.restaurantapp.business.interactors.abstraction.GetOrders
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetEntities2Impl"
private const val LOG_ME = true

class GetOrdersImpl
@Inject
constructor(
    private val cacheDataSource: OrderCacheDataSource,
    private val networkDataSource: OrderNetworkDataSource,
    private val entityFactory: OrderFactory
): GetOrders {
    override fun getEntities2(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<Order>?) -> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val userTrips = safeApiCall(
            dispatcher = Dispatchers.IO,
            onErrorAction = onErrorAction,
            apiCall = {
                returnViewState.mainEntity?.id?.let { networkDataSource.getUsersEntities2(it) }
            }
        )

        val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<Order>>(
            response = userTrips,
            stateEvent = stateEvent,
        ) {
            override suspend fun handleSuccess(resultObj: List<Order>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GetUsersTripsImplConstants.GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d(TAG, "getEntities2().handleSuccess(): ")


                if(resultObj == null){
                    ALog.d(TAG, "getEntities2(): resultObj == null")
                    message = GetUsersTripsImplConstants.GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d(TAG, "getEntities2(): resultObj != null")
                    returnViewState.dishOrdersList = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    object GetUsersTripsImplConstants{
        const val GET_ENTITY_SUCCESS = "Successfully entities 2."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities 2 in database."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities 2."
    }
}
