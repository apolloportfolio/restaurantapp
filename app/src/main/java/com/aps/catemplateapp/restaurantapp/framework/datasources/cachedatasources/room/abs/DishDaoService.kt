package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish

interface DishDaoService {
    suspend fun insertOrUpdateEntity(entity: Dish): Dish

    suspend fun insertEntity(entity: Dish): Long

    suspend fun insertEntities(Entities: List<Dish>): LongArray

    suspend fun getEntityById(id: UniqueID?): Dish?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        price: Double?,
        currency: String?,
        allergenInfo: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<Dish>): Int

    suspend fun searchEntities(): List<Dish>

    suspend fun getAllEntities(): List<Dish>

    suspend fun getNumEntities(): Int
}