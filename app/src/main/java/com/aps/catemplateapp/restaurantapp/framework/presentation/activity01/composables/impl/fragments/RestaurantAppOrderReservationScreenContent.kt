package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Send
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BackgroundsWithGradientsImpl
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.DateInputType01
import com.aps.catemplateapp.common.framework.presentation.views.NumberInputType01
import com.aps.catemplateapp.common.framework.presentation.views.TimeInputType01
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsRestaurantApp
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Shapes
import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun RestaurantAppOrderReservationScreenContent(
    projectUser: ProjectUser?,
    onReservationConfirmed: (Reservation) -> Unit,
) {
    var selectedDate by remember { mutableStateOf("") }
    var selectedTime by remember { mutableStateOf("") }
    var numberOfGuests by remember { mutableStateOf("") }

    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        if(projectUser == null) {
            TipContentIsUnavailable(tip = stringResource(R.string.no_content_to_show_due_to_an_error))
        } else {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(
                        start = Dimens.spacingMedium,
                        end = Dimens.spacingMedium,
                        top = Dimens.spacingMedium,
                        bottom = Dimens.spacingMedium,
                    )
            ) {
                val (
                    banner, title, datePick, timePick, numberOfGuestsPick, confirmReservationButton,
                ) = createRefs()

                // Banner
                Image(
                    modifier = Modifier
                        .constrainAs(banner) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .fillMaxWidth()
                        .clip(Shapes.medium)
                        .padding(
                            start = Dimens.spacingMedium,
                            end = Dimens.spacingMedium,
                            top = Dimens.spacingMedium,
                            bottom = Dimens.spacingMedium,
                        )
                        .shadow(
                            elevation = Dimens.elevationSmall,
                            shape = Shapes.medium
                        ),
                    painter = fallbackPainterResource(R.drawable.restaurant_app_banner),
                    contentDescription = stringResource(id = R.string.restaurant_app_banner),
                    contentScale = ContentScale.FillWidth
                )

                val titleBackgroundColors = listOf(
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                )

                val pickersColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primary,
                )

                // Title label
                TitleRowType01(
                    titleString = stringResource(id = R.string.make_reservation),
                    textAlign = TextAlign.Center,
//                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsWithGradientsImpl.backgroundHorizontalGradient(
                        colors = titleBackgroundColors,
                        radius = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    leftPictureDrawableId = null,
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                    modifier = Modifier
                        .constrainAs(title) {
                            top.linkTo(banner.bottom)
                            bottom.linkTo(datePick.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .fillMaxWidth()
                )

                // Date Picker
                DateInputType01(
                    selectedDate = selectedDate,
                    onDateSelected = { date -> selectedDate = date },
                    showDatePicker = { /* Functionality unavailable in demo project */ },
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundListItemType01(
                        colors = pickersColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    modifier = Modifier
                        .constrainAs(datePick) {
                            top.linkTo(title.bottom)
                            bottom.linkTo(timePick.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                )

                Spacer(modifier = Modifier.height(Dimens.spacingMedium))

                // Time Picker
                TimeInputType01(
                    selectedTime = selectedTime,
                    onTimeSelected = { time -> selectedTime = time },
                    showTimePicker = { /* Functionality unavailable in demo project */ },
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundListItemType01(
                        colors = pickersColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    modifier = Modifier
                        .constrainAs(timePick) {
                            top.linkTo(datePick.bottom)
                            bottom.linkTo(numberOfGuestsPick.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                )

                Spacer(modifier = Modifier.height(Dimens.spacingMedium))

                // Number of Guests Input
                NumberInputType01(
                    numberOfGuests = numberOfGuests,
                    onNumberSelected = { number -> numberOfGuests = number },
                    showNumberPicker = { /* Functionality unavailable in demo project */},
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundListItemType01(
                        colors = pickersColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    modifier = Modifier
                        .constrainAs(numberOfGuestsPick) {
                            top.linkTo(timePick.bottom)
                            bottom.linkTo(confirmReservationButton.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                )

                Spacer(modifier = Modifier.height(Dimens.spacingMedium))

                // Confirm Reservation Button
                Button(
                    onClick = {
                        val timestampNow = DateUtil.convertFirebaseTimestampToStringData(Timestamp.now())
                        onReservationConfirmed(
                            Reservation(
                                id = null,
                                created_at = timestampNow,
                                updated_at = timestampNow,
                                picture1URI = null,
                                description = null,
                                ownerID = projectUser.id,
                                date = selectedDate,
                                time = selectedTime,
                                numberOfGuests = numberOfGuests,
                            )
                        )
                    },
                    modifier = Modifier
                        .constrainAs(confirmReservationButton) {
                            bottom.linkTo(parent.bottom)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .fillMaxWidth()
                        .height(80.dp)
                        .padding(
                            top = Dimens.spacingMedium,
                            bottom = Dimens.spacingSmall,
                        )
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(Dimens.defaultPadding),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = stringResource(id = R.string.confirm_reservation),
                            style = MaterialTheme.typography.button
                        )
                        Icon(
                            imageVector = Icons.Outlined.Send,
                            contentDescription = null
                        )
                    }
                }
            }
        }
    }
}

// Preview =========================================================================================
@Preview
@Composable
fun RestaurantAppOrderReservationScreenContentPreview() {
    HomeScreenTheme {
        val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
        val dateFormatForUser = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        val dateFormatJustYear = SimpleDateFormat("yyyy", Locale.ENGLISH)
        val dateUtil = DateUtil(dateFormatForFirestore, dateFormatForUser, dateFormatJustYear)
        val entityFactory = UserFactory(dateUtil)
        RestaurantAppOrderReservationScreenContent(
            onReservationConfirmed = {},
            projectUser = entityFactory.generateEmpty()
        )
    }
}
