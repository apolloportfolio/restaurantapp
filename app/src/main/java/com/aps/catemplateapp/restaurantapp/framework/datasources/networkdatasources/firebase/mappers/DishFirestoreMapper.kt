package com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.model.DishFirestoreEntity
import javax.inject.Inject

class DishFirestoreMapper
@Inject
constructor() : EntityMapper<DishFirestoreEntity, Dish> {
    override fun mapFromEntity(entity: DishFirestoreEntity): Dish {
        val dish = Dish(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.lattitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,

            entity.price,
            entity.currency,
            entity.allergenInfo,
        )
        if(entity.geoLocation != null){
            dish.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return dish
    }

    override fun mapToEntity(domainModel: Dish): DishFirestoreEntity {
        val entity1NetworkEntity =
        DishFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,
            
            domainModel.city,

            null,           //keywords
            
            domainModel.ownerID,

            domainModel.name,
            
            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,

            domainModel.price,
            domainModel.currency,
            domainModel.allergenInfo,
        )
        return entity1NetworkEntity.generateKeywords()
    }

    override fun mapFromEntityList(entities : List<DishFirestoreEntity>) : List<Dish> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Dish>): List<DishFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}