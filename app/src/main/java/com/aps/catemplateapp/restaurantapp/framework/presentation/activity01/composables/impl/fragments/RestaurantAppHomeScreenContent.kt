package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material3.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ImageFromFirebase
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType03
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.DishFactory
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsRestaurantApp
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Type

@Composable
fun RestaurantAppHomeScreenContent(
    dishes: List<Dish>,
    selectedDish: Dish?,
    onDishSelected: (Dish) -> Unit,
    onOrderButtonClick: (Dish) -> Unit,
    onReservationButtonClick: () -> Unit,

    onListItemClick: (Dish) -> Unit,
    launchInitStateEvent: () -> Unit,

    isPreview: Boolean = false,
) {
    val transparentColor = remember {
        Color(0x00000000)
    }
    val startMargin = remember { 4 }.dp
    val endMargin = remember { 4 }.dp
    val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)

    var shownDish by remember {
        mutableStateOf(selectedDish)
    }
    if(shownDish == null)shownDish = dishes.randomOrNull()
    if(shownDish == null) {
        TipContentIsUnavailable(tip = stringResource(id = R.string.no_content_to_show_due_to_an_error))
    } else {
        BoxWithBackground(
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            )
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(Dimens.columnPadding)
            ) {
                // Header
                val headerBackgroundColors = listOf(
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                )
                TitleRowType01(
                    titleString = stringResource(id = R.string.choose_your_delight),
                    textAlign = TextAlign.Center,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundTitleType01(
                        colors = headerBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                    modifier = Modifier
                        .testTag("AppHeader"),
                )

                // Display the selected dish or a random dish if none is selected
                val chosenDishBackgroundColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primary,
                )
                var shownDishPrice = ""
                if(shownDish!!.price != null) {
                    shownDishPrice = String.format("%.2f", shownDish!!.price) + shownDish!!.currency
                } else if(shownDish!!.currency != null) {
                    shownDishPrice += shownDish!!.currency
                }
                BoxWithBackground(
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
                        colors = chosenDishBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    )
                ) {
                    if(isPreview) {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                        ) {
                            Text(
                                text = selectedDish?.name ?: stringResource(id = R.string.random_dish),
                                style = Type.h4.copy(textAlign = TextAlign.Center),
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(Dimens.defaultPadding)
                                    .testTag("SelectedDishName"),
                                color = MaterialTheme.colors.secondary,
                            )
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Box {
                                    Image(
                                        painter = fallbackPainterResource(id = R.drawable.restaurant_app_example_dish_9),
                                        contentDescription = stringResource(R.string.chosen_dish),
                                        modifier = Modifier
                                            .align(Alignment.Center)
                                            .height(200.dp)
                                            .width(200.dp)
                                            .background(transparentColor)
                                            .padding(
                                                start = startMargin,
                                                end = endMargin,
                                                top = topMargin,
                                                bottom = bottomMargin,
                                            )
                                            .clickable(onClick = {
                                                selectedDish?.let {
                                                    onListItemClick(
                                                        it
                                                    )
                                                }
                                            }),
                                        alignment = Alignment.Center,
                                        contentScale = ContentScale.Fit,
                                        alpha = DefaultAlpha,
                                        colorFilter = null,
                                    )


                                    Text(
                                        text = shownDishPrice,
                                        style = Type.h4,
                                        modifier = Modifier
                                            .align(Alignment.BottomEnd)
                                            .padding(Dimens.defaultPadding)
                                            .testTag("SelectedDishPrice"),
                                        color = MaterialTheme.colors.secondary,
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(12.dp))
                        }
                    } else {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                        ) {
                            Text(
                                text = selectedDish?.name ?: stringResource(id = R.string.random_dish),
                                style = Type.h4.copy(textAlign = TextAlign.Center),
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(Dimens.defaultPadding)
                                    .testTag("SelectedDishName"),
                                color = MaterialTheme.colors.secondary,
                            )
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Box {
                                    ImageFromFirebase(
                                        firebaseImageRef = shownDish!!.picture1FirebaseImageRef,
                                        modifier = Modifier
                                            .height(200.dp)
                                            .width(200.dp)
                                            .background(transparentColor)
                                            .padding(
                                                start = startMargin,
                                                end = endMargin,
                                                top = topMargin,
                                                bottom = bottomMargin,
                                            )
                                            .fillMaxWidth(),
                                        contentDescription = stringResource(R.string.chosen_dish),
                                        contentScale = ContentScale.Fit
                                    )

                                    Text(
                                        text = selectedDish?.price?.toString() ?: "",
                                        style = Type.body1,
                                        modifier = Modifier
                                            .align(Alignment.BottomEnd)
                                            .padding(Dimens.defaultPadding)
                                            .testTag("SelectedDishPrice"),
                                        color = MaterialTheme.colors.secondary,
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(12.dp))
                        }

                    }
                }

                // List of dishes
                val dishBackgroundColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primary,
                )
                LazyColumn(
                    Modifier.padding(Dimens.columnPadding)
                ) {
                    itemsIndexed(dishes) { index, dish ->
                        // Each dish item with an image, name, and price
                        var price = ""
                        if(dish.price != null) {
                            price = String.format("%.2f", dish.price) + dish.currency
                        } else if(dish.currency != null) {
                            price += dish.currency
                        }
                        ListItemType03(
                            index = index,
                            itemTitleString = dish.name,
                            itemPriceString = price,
                            itemLikesString = "",
                            itemDistanceString = "",
                            lblSmallShortAttribute3String = "",
                            itemDescriptionString = dish.description,
                            onListItemClick = {
                                shownDish = dish
                                onDishSelected(dish)
                                onListItemClick(dish)
                            },
                            getItemsRating = { null },
                            itemRef = if(isPreview) {
                                null
                            } else {
                                dish.picture1FirebaseImageRef
                            },
                            itemDrawableId = if(isPreview) {
                                getDrawableIdInPreview("restaurant_app_example_dish_", index)
                            } else { null },
                            imageTint = null,
                            backgroundDrawableId = R.drawable.example_background_1,
                            composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
                                colors = dishBackgroundColors,
                                brush = null,
                                shape = null,
                                alpha = 0.6f,
                            ),
                        )
                    }
                }

                // Order and Reservation buttons
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(Dimens.defaultPadding),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Button(
                        onClick = { onOrderButtonClick(shownDish!!) },
                        modifier = Modifier
                            .weight(1f)
                            .padding(Dimens.defaultPadding)
                            .testTag("OrderButton")
                    ) {
                        Text(stringResource(id = R.string.order))
                    }

                    Button(
                        onClick = onReservationButtonClick,
                        modifier = Modifier
                            .weight(1f)
                            .testTag("ReservationButton")
                    ) {
                        Text(stringResource(id = R.string.reservation))
                    }
                }
            }
        }
    }
}

// Preview =========================================================================================
@Preview
@Composable
fun RestaurantAppHomeScreenContentPreview() {
    val dishes = DishFactory.createPreviewEntitiesList()
    val selectedDish = dishes.lastOrNull()

    HomeScreenTheme {
        RestaurantAppHomeScreenContent(
            dishes = DishFactory.createPreviewEntitiesList(),
            selectedDish = selectedDish,
            onDishSelected = {},
            onOrderButtonClick = {},
            onReservationButtonClick = {},
            onListItemClick = {},
            launchInitStateEvent = {},
            isPreview = true,
        )
    }
}
