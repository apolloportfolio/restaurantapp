package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.runtime.Composable
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenStateEvent


@Composable
fun HomeScreenCompDetailsScreen3(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},
    projectUser: ProjectUser?,
    navigateToProfileScreen: () -> Unit,
) {
    RestaurantAppOrderReservationScreenContent(
        onReservationConfirmed = {
            launchStateEvent(HomeScreenStateEvent.ConfirmReservation(
                reservation = it,
                callbackOnProfileIncomplete = navigateToProfileScreen,
            ))
        },
        projectUser = projectUser,
    )
}