package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.ReservationDaoService
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.ReservationDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.mappers.ReservationCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReservationDaoServiceImpl
@Inject
constructor(
    private val dao: ReservationDao,
    private val mapper: ReservationCacheMapper,
    private val dateUtil: DateUtil
): ReservationDaoService {

    override suspend fun insertOrUpdateEntity(entity: Reservation): Reservation {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.picture1URI,

                entity.description,

                entity.ownerID,

                entity.date,
                entity.time,
                entity.numberOfGuests,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: Reservation): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<Reservation>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): Reservation? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        date: String?,
        time: String?,
        numberOfGuests: String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                picture1URI,

                description,

                ownerID,

                date,
                time,
                numberOfGuests,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                picture1URI,

                description,

                ownerID,

                date,
                time,
                numberOfGuests,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Reservation>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<Reservation> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<Reservation> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}