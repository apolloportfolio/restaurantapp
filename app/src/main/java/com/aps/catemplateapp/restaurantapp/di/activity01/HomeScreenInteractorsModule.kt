package com.aps.catemplateapp.restaurantapp.di.activity01

import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.catemplateapp.common.business.interactors.implementation.DoNothingAtAllImpl
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.DishCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.OrderCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.ReservationCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.DishNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.OrderNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.ReservationNetworkDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.DishFactory
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.OrderFactory
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.ReservationFactory
import com.aps.catemplateapp.core.util.SecureKeyStorage
import com.aps.catemplateapp.restaurantapp.business.interactors.abstraction.*
import com.aps.catemplateapp.restaurantapp.business.interactors.implementation.*
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object HomeScreenInteractorsModule {

    @Provides
    fun provideGetUsersRating(
        cacheDataSource: UserCacheDataSource,
        networkDataSource: UserNetworkDataSource,
    ): GetUsersRating {
        return GetUsersRatingImpl(cacheDataSource, networkDataSource)
    }

    @Provides
    fun InitiateGooglePayPaymentProcess(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): InitiateGooglePayPaymentProcess {
        return InitiateGooglePayPaymentProcessImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetMerchantName(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetMerchantName {
        return GetMerchantNameImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetGatewayNameAndMerchantID(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetGatewayNameAndMerchantID {
        return GetGatewayNameAndMerchantIDImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideDownloadExchangeRates(): DownloadExchangeRates {
        return DownloadExchangeRatesImpl()
    }

    @Provides
    fun provideCheckGooglePayAvailability(): CheckGooglePayAvailability {
        return CheckGooglePayAvailabilityImpl()
    }

    @Provides
    fun provideLogoutUser(
        userCacheDataSource: UserCacheDataSource,
        userNetworkDataSource: UserNetworkDataSource,
    ): LogoutUser {
        return LogoutUserImpl(
            userCacheDataSource,
            userNetworkDataSource,
        )
    }

    @Provides
    fun provideGetEntities1AroundUser(
        cacheDataSource: DishCacheDataSource,
        networkDataSource: DishNetworkDataSource,
        entityFactory: DishFactory
    ): GetDishesAroundUser {
        return GetDishesAroundUserImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }


    @Provides
    fun provideGetEntities2(
        cacheDataSource: OrderCacheDataSource,
        networkDataSource: OrderNetworkDataSource,
        entityFactory: OrderFactory
    ): GetOrders {
        return GetOrdersImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideGetEntities3(
        cacheDataSource: ReservationCacheDataSource,
        networkDataSource: ReservationNetworkDataSource,
        entityFactory: ReservationFactory
    ): GetReservations {
        return GetReservationsImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideSearchDishes(
        cacheDataSource: DishCacheDataSource,
        networkDataSource: DishNetworkDataSource,
        entityFactory: DishFactory
    ): SearchDishes {
        return SearchDishesImpl(
            cacheDataSource,
            networkDataSource,
        )
    }

    @Provides
    fun provideSearchDishOrders(
        cacheDataSource: OrderCacheDataSource,
        networkDataSource: OrderNetworkDataSource,
        entityFactory: OrderFactory
    ): SearchDishOrders {
        return SearchDishOrdersImpl(
            cacheDataSource,
            networkDataSource,
        )
    }

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>> {
        return DoNothingAtAllImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>>()
    }
}