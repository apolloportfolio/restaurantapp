package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ImageFromFirebase
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsRestaurantApp
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Type


@Composable
fun RestaurantAppItemDetailsScreenContent(
    dish: Dish,
    onAddToCartClick: () -> Unit,
    onBackClick: () -> Unit,
    navigateToProfileScreen: () -> Unit,
    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.5f,
        )
    ) {
        val transparentColor = remember {
            Color(0x00000000)
        }
        val startMargin = remember { 4 }.dp
        val endMargin = remember { 4 }.dp
        val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
        val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)

        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    start = startMargin,
                    end = endMargin,
                    top = topMargin,
                    bottom = bottomMargin,
                )
        ) {
            val (
                itemImage, itemTitle, itemPrice, itemDescription, bottomButtons,
            ) = createRefs()

            // Dish Image
            if(dish.picture1FirebaseImageRef != null) {
                ImageFromFirebase(
                    firebaseImageRef = dish.picture1FirebaseImageRef,
                    modifier = Modifier
                        .constrainAs(itemImage) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        )
                        .fillMaxHeight(0.3F)
                        .fillMaxWidth(),
                    contentDescription = dish.name ?: "",
                    contentScale = ContentScale.Crop,
                )
            } else if(isPreview) {
                Image(
                    painter = fallbackPainterResource(id = R.drawable.restaurant_app_example_dish_4),
                    contentDescription = dish.name ?: "",
                    modifier = Modifier
                        .constrainAs(itemImage) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .background(transparentColor)
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        )
                        .fillMaxHeight(0.3F)
                        .fillMaxWidth(),
//                    alignment = Alignment.Center,
                    contentScale = ContentScale.Crop,
                    alpha = DefaultAlpha,
                    colorFilter = null,
                )
            }


            // Dish Details
            Text(
                text = dish.name ?: "",
                style = Type.h5,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .constrainAs(itemTitle) {
                        top.linkTo(itemImage.bottom)
                        start.linkTo(parent.start)
                    }
                    .padding(
                        start = Dimens.spacingMedium,
                        end = Dimens.spacingMedium,
                        top = topMargin,
                        bottom = bottomMargin
                    )
                    .fillMaxWidth(),
            )
            Text(
                text = buildAnnotatedString {
                    withStyle(style = SpanStyle(color = Color.Gray)) {
                        append("Price: ")
                    }
                    append("${dish.price} ${dish.currency}")
                },
                style = Type.body1,
                modifier = Modifier
                    .constrainAs(itemPrice) {
                        top.linkTo(itemTitle.bottom)
                        start.linkTo(parent.start)
                    }
                    .padding(
                        start = Dimens.spacingMedium,
                        end = Dimens.spacingMedium,
                        top = topMargin,
                        bottom = bottomMargin
                    )
                    .fillMaxWidth(),
            )

            // Dish description
            Column(
                modifier = Modifier
                    .constrainAs(itemDescription) {
                        top.linkTo(itemPrice.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        bottom.linkTo(bottomButtons.top)
                    }
                    .padding(
                        start = Dimens.spacingMedium,
                        end = Dimens.spacingMedium,
                        top = topMargin,
                        bottom = bottomMargin
                    )
                    .fillMaxHeight(0.5F)
                    .fillMaxWidth(),
            ) {
                Text(
                    text = dish.description ?: "",
                    style = Type.body2,
                    modifier = Modifier.padding(bottom = Dimens.spacingSmall)
                )
                Text(
                    text = dish.allergenInfo ?: "",
                    style = Type.caption,
                    modifier = Modifier.padding(bottom = Dimens.spacingLarge)
                )
            }

            // Buttons at the bottom
            Row(
                modifier = Modifier
                    .constrainAs(bottomButtons) {
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .padding(
                        start = Dimens.spacingMedium,
                        end = Dimens.spacingMedium,
                        top = Dimens.spacingMedium,
                        bottom = Dimens.spacingMedium
                    )
                    .fillMaxWidth()
                    .height(56.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                // Add to Cart Button
                Button(
                    onClick = { onAddToCartClick() },
                    modifier = Modifier
                        .fillMaxWidth(0.7F)
                        .height(56.dp),

                ) {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = null,
                    )
                    Spacer(modifier = Modifier.width(Dimens.spacingSmall))
                    Text(
                        text = stringResource(id = R.string.add_to_cart),
                        style = Type.button,
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onPrimary,
                    )
                }

                // Back Button
                val backButtonContentDescription = stringResource(id = R.string.back_button)
                IconButton(
                    onClick = { onBackClick() },
                    modifier = Modifier
                        .padding(start = Dimens.spacingSmall, end = Dimens.spacingSmall)
                        .width(56.dp)
                        .height(56.dp)
                        .semantics { contentDescription = backButtonContentDescription },
                ) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = null,
                    )
                }
            }
        }

    }
}

// Preview
@Preview
@Composable
fun RestaurantAppItemDetailsScreenContentPreview() {
    val sampleDish = Dish(
        id = UniqueID("123"),
        created_at = "2023-01-01",
        updated_at = "2023-01-02",
        latitude = 37.7749,
        longitude = -122.4194,
        geoLocation = ParcelableGeoPoint(37.7749, -122.4194),
        firestoreGeoLocation = null,
        picture1URI = null,
        description = "Indulge your senses in the extraordinary flavors of our signature dish, a culinary masterpiece that harmoniously blends the finest ingredients to create an unforgettable dining experience. Immerse yourself in the rich symphony of tastes and textures, where succulent pieces of premium, hand-selected meat are expertly seasoned and slow-cooked to perfection, creating a melt-in-your-mouth sensation that is both satisfying and luxurious.\n" +
                "\n" +
                "Our chef's creative genius shines through in every aspect of this culinary creation, from the delicate balance of spices to the meticulous attention to detail in the presentation. Picture a beautifully plated dish featuring a medley of vibrant, farm-fresh vegetables that have been artfully prepared to enhance both their natural sweetness and crispiness. Each bite is a journey through a spectrum of flavors, from the savory umami notes of the protein to the refreshing crunch of perfectly cooked vegetables.\n" +
                "\n" +
                "As you savor this gastronomic delight, you'll discover an intricate interplay of aromas that captivate your senses, inviting you to appreciate the nuanced blend of herbs and seasonings that elevate this dish to a level of culinary excellence. The sauce, a velvety symphony of complementary flavors, ties everything together, adding depth and character to each mouthful.\n" +
                "\n" +
                "Whether you're a seasoned food enthusiast or a casual diner seeking an extraordinary dining adventure, this dish promises to be a revelation. It's not just a meal; it's an exploration of taste, a celebration of culinary artistry that transcends the ordinary. Join us on a gastronomic journey and treat yourself to an experience that is nothing short of extraordinary.",
        city = "San Francisco",
        ownerID = UserUniqueID("456"),
        name = "Dinner Of The Gods",
        switch1 = true,
        switch2 = false,
        switch3 = true,
        switch4 = false,
        switch5 = true,
        switch6 = false,
        switch7 = true,
        price = 19.99,
        currency = "USD",
        allergenInfo = "Contains nuts and dairy."
    )

    HomeScreenTheme{
        RestaurantAppItemDetailsScreenContent(
            dish = sampleDish,
            onAddToCartClick = {},
            onBackClick = {},
            navigateToProfileScreen = {},
            isPreview = true,
        )
    }
}
