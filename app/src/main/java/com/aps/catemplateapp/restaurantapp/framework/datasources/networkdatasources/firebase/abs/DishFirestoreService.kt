package com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs

import android.net.Uri
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.business.interactors.implementation.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.mappers.DishFirestoreMapper
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.model.DishFirestoreEntity

interface DishFirestoreService: BasicFirestoreService<
        Dish,
        DishFirestoreEntity,
        DishFirestoreMapper
        > {
    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<Dish>?

    suspend fun getUsersEntities1(userID: UserUniqueID): List<Dish>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : Dish,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}