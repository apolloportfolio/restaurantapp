package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order

interface OrderDaoService {
    suspend fun insertOrUpdateEntity(entity: Order): Order

    suspend fun insertEntity(entity: Order): Long

    suspend fun insertEntities(Entities: List<Order>): LongArray

    suspend fun getEntityById(id: UniqueID?): Order?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        orderedDishesIds: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<Order>): Int

    suspend fun searchEntities(): List<Order>

    suspend fun getAllEntities(): List<Order>

    suspend fun getNumEntities(): Int
}