package com.aps.catemplateapp.restaurantapp.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.DishCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.DishDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DishCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: DishDaoService
): DishCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: Dish): Dish? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: Dish): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Dish>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        price: Double?,
        currency: String?,
        allergenInfo: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,
            city,
            ownerID,
            name,

            price,
            currency,
            allergenInfo,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Dish> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<Dish> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): Dish? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<Dish>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}