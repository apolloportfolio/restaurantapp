package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.layouts

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen1
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen2
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen3
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen4
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen5
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreenPlaceholder
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment1
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment1BottomSheetActions
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment2
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment2BottomSheetActions
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment3
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment3BottomSheetActions
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment4
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment4BottomSheetActions
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment5
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment5BottomSheetActions
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "LeftPaneNavGraphBuilder"
private const val LOG_ME = true
@Composable
internal fun LeftPaneNavGraphBuilder(
    launchStateEvent: (HomeScreenStateEvent) -> Unit,
    onReservationConfirmed: (Reservation) -> Unit,

    projectUser: ProjectUser?,

    activity: Activity?,
    permissionHandlingData: PermissionHandlingData,

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    navigateCardLeft: () -> Unit,
    navigateCardRight: () -> Unit,
    openDrawer: () -> Unit,
    navigateToProfileScreen: () -> Unit,
    onBackButtonPress: () -> Unit,

    floatingActionButtonDrawableIdCard1: Int? = null,
    floatingActionButtonOnClickCard1: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard1: String? = null,
    card1EntitiesList: List<Dish>?,
    card1OnListItemClick: (Dish) -> Unit,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    makeReservation: () -> Unit,
    card1SearchQuery: String,
    card1OnSearchQueryUpdate: (String) -> Unit,
    searchFilters: HomeScreenCard1SearchFilters,
    onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,
    card1BottomSheetActions: HomeScreenComposableFragment1BottomSheetActions,
    card1CurrentlyShownEntity: Dish? = null,
    card1ActionOnEntity: (Dish?, ()-> Unit) -> Unit,

    floatingActionButtonDrawableIdCard2: Int? = null,
    floatingActionButtonOnClickCard2: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard2: String? = null,
    card2EntitiesList: List<Order>?,
    card2OnListItemClick: (Order) -> Unit,
    card2SearchQuery: String,
    card2OnSearchQueryUpdate: (String) -> Unit,
    card2BottomSheetActions: HomeScreenComposableFragment2BottomSheetActions,

    floatingActionButtonDrawableIdCard3: Int? = null,
    floatingActionButtonOnClickCard3: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard3: String? = null,
    card3EntitiesList: List<Reservation>?,
    card3OnListItemClick: (Reservation) -> Unit,
    card3BottomSheetActions: HomeScreenComposableFragment3BottomSheetActions,

    floatingActionButtonDrawableIdCard4: Int? = null,
    floatingActionButtonOnClickCard4: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard4: String? = null,
    card4EntitiesList: List<Dish>?,
    card4OnListItemClick: (Dish) -> Unit,
    card4BottomSheetActions: HomeScreenComposableFragment4BottomSheetActions,

    card5BottomSheetActions: HomeScreenComposableFragment5BottomSheetActions,

    isPreview: Boolean = false,
): NavGraphBuilder.() -> Unit {
    return {
        composable(HomeScreenDestination.Card1.route) {
            HomeScreenCompFragment1(
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                makeReservation = makeReservation,
                initialSearchQuery = card1SearchQuery,
                entities = card1EntitiesList,
                onSearchQueryUpdate = card1OnSearchQueryUpdate,
                onListItemClick = card1OnListItemClick,
                searchFilters = searchFilters,
                onSearchFiltersUpdated = onSearchFiltersUpdated,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard1,
                floatingActionButtonOnClick = floatingActionButtonOnClickCard1,
                floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard1,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card1BottomSheetActions,
                isPreview = isPreview,
            )
        }
        composable(HomeScreenDestination.Card2.route) {
            HomeScreenCompFragment2(
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                entities = card2EntitiesList,
                onListItemClick = card2OnListItemClick,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard2,
                floatingActionButtonOnClick = floatingActionButtonOnClickCard2,
                floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard2,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card2BottomSheetActions,

                initialSearchQuery = card2SearchQuery,
                onSearchQueryUpdate = card2OnSearchQueryUpdate,
                isPreview = isPreview,
            )
        }
        composable(HomeScreenDestination.Card3.route) {
            HomeScreenCompFragment3(
                launchStateEvent = launchStateEvent,
                projectUser = projectUser,
                onReservationConfirmed = onReservationConfirmed,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                entities = card3EntitiesList,
                onListItemClick = card3OnListItemClick,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard3,
                floatingActionButtonOnClick = floatingActionButtonOnClickCard3,
                floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard3,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card3BottomSheetActions,
            )
        }
        composable(HomeScreenDestination.Card4.route) {
            HomeScreenCompFragment4(
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                entities = card4EntitiesList,
                onListItemClick = card4OnListItemClick,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard4,
                floatingActionButtonOnClick = floatingActionButtonOnClickCard4,
                floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard4,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card4BottomSheetActions,
            )
        }
        composable(HomeScreenDestination.Card5.route) {
            HomeScreenCompFragment5(
                launchStateEvent = launchStateEvent,
                projectUser = projectUser,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card5BottomSheetActions,
                onBackButtonPress = onBackButtonPress,

                isPreview = isPreview,
            )
        }
        composable(HomeScreenDestination.DetailsScreen1.route) {
            HomeScreenCompDetailsScreen1(
                launchStateEvent = launchStateEvent,

                activity = activity,
                permissionHandlingData = permissionHandlingData,

                stateEventTracker = stateEventTracker,

                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,

                onBackButtonPress = onBackButtonPress,

                entity = card1CurrentlyShownEntity,
                actionOnEntity = card1ActionOnEntity,
                navigateToProfileScreen = navigateToProfileScreen,
            )
        }
        composable(HomeScreenDestination.DetailsScreen2.route) {
            HomeScreenCompDetailsScreen2()
        }
        composable(HomeScreenDestination.DetailsScreen3.route) {
            HomeScreenCompDetailsScreen3(
                launchStateEvent = launchStateEvent,
                projectUser = projectUser,
                navigateToProfileScreen = finishVerification,
            )
        }
        composable(HomeScreenDestination.DetailsScreen4.route) {
            HomeScreenCompDetailsScreen4()
        }
        composable(HomeScreenDestination.DetailsScreen5.route) {
            HomeScreenCompDetailsScreen5()
        }
        composable(HomeScreenDestination.DetailsPlaceholderScreen.route) {
            HomeScreenCompDetailsScreenPlaceholder()
        }
    }
}