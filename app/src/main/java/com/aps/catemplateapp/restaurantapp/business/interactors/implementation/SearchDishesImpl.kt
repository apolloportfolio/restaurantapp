package com.aps.catemplateapp.restaurantapp.business.interactors.implementation

import android.location.Location
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.DishCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.DishNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.interactors.abstraction.SearchDishes
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "SearchEntities1Impl"
private const val LOG_ME = true

class SearchDishesImpl
@Inject
constructor(
    private val dishCacheDataSource: DishCacheDataSource,
    private val dishNetworkDataSource: DishNetworkDataSource,
): SearchDishes {
    override fun searchDishes(
        location : Location?,
        searchParameters : HomeScreenViewState<ProjectUser>,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<Dish>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val syncedEntities = safeCacheCall(Dispatchers.IO, onErrorAction = onErrorAction){
            if(LOG_ME) ALog.d(TAG, ".searchEntities1(): searchParameters: \n$searchParameters")
            syncEntities(location, searchParameters)
        }


        val response = object: CacheResponseHandler<HomeScreenViewState<ProjectUser>, List<Dish>>(
            response = syncedEntities,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: List<Dish>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GET_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if(LOG_ME)ALog.d(TAG, "searchEntities1().handleSuccess(): ")


                if(resultObj == null){
                    if(LOG_ME)ALog.d(TAG, "searchEntities1(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    if(LOG_ME)ALog.d(TAG, "searchEntities1(): resultObj != null")
                    returnViewState.searchedDishesList = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    // Method downloads entities from network and saves them in cache
    private suspend fun syncEntities(
        location : Location?,
        searchParameters : HomeScreenViewState<ProjectUser>,
    ) : List<Dish> {
        val firestoreSearchParameters = FirestoreEntity1SearchParameters(searchParameters, location)
        val networkEntitiesList = getNetworkEntities(firestoreSearchParameters)
        return networkEntitiesList
    }

    private suspend fun getNetworkEntities(searchParameters : FirestoreEntity1SearchParameters): List<Dish>{
        val networkResult = safeApiCall(Dispatchers.IO){
            if(LOG_ME)ALog.d(TAG, ".getNetworkEntities(): searchParameters: \n$searchParameters")
            dishNetworkDataSource.searchEntities(searchParameters)
        }

        val response = object: ApiResponseHandler<List<Dish>, List<Dish>>(
            response = networkResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<Dish>): DataState<List<Dish>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    companion object {
        const val GET_ENTITIES_SUCCESS = "Successfully searched entities."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."
    }
}
