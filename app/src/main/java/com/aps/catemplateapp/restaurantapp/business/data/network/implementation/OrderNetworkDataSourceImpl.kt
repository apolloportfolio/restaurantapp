package com.aps.catemplateapp.restaurantapp.business.data.network.implementation

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.OrderNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs.OrderFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: OrderFirestoreService
): OrderNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Order): Order? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: Order) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<Order>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: Order) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<Order> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: Order): Order? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<Order> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<Order>): List<Order>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): Order? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun searchEntities(
        dishName: String,
    ) : List<Order>? {
        return firestoreService.searchEntities(dishName)
    }

    override suspend fun getUsersEntities2(userId : UserUniqueID) : List<Order>? {
        return firestoreService.getUsersEntities2(userId)
    }
}