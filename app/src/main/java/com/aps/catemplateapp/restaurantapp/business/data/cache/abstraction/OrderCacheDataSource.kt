package com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order

interface OrderCacheDataSource: StandardCacheDataSource<Order> {
    override suspend fun insertOrUpdateEntity(entity: Order): Order?

    override suspend fun insertEntity(entity: Order): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<Order>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        orderedDishesIds: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Order>

    override suspend fun getAllEntities(): List<Order>

    override suspend fun getEntityById(id: UniqueID?): Order?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<Order>): LongArray
}