package com.aps.catemplateapp.restaurantapp.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.OrderCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.OrderDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: OrderDaoService
): OrderCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: Order): Order? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: Order): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Order>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        orderedDishesIds: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            description,

            ownerID,

            orderedDishesIds,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Order> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<Order> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): Order? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<Order>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}