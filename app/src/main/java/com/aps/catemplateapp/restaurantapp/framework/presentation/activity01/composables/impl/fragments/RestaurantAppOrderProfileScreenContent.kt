package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Description
import androidx.compose.material.icons.filled.LocationCity
import androidx.compose.material.icons.filled.MailOutline
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Update
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ImageFromFirebase
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.UserInfoItem
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsRestaurantApp
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenStateEvent


@Composable
fun RestaurantAppOrderProfileScreenContent(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    user: ProjectUser?,
    onBackClick: () -> Unit,

    isPreview: Boolean,
) {
    var isEditing by remember { mutableStateOf(false) }

    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.5f,
        )
    ) {
        val startMargin = remember { 4 }.dp
        val endMargin = remember { 4 }.dp
        val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
        val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)

        if(user == null) {
            TipContentIsUnavailable(tip = stringResource(R.string.no_content_to_show_due_to_an_error))
        } else {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(
                        start = Dimens.spacingMedium,
                        end = Dimens.spacingMedium,
                        top = Dimens.spacingMedium,
                        bottom = Dimens.spacingMedium,
                    )
            ) {
                val (userInfo, profileScreenButtonRow,) = createRefs()

                UserInfo(
                    modifier = Modifier
                        .constrainAs(userInfo) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        },
                    user = user,
                    isEditing = isEditing,
                    onDoneClick = { isEditing = false },
                    isPreview = isPreview,
                )

                ProfileScreenButtonRow(
                    modifier = Modifier
                        .constrainAs(profileScreenButtonRow) {
                            bottom.linkTo(parent.bottom)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        },
                    isEditing,
                    onBackClick,
                    onEditClick = {
                        isEditing = !isEditing
                    },
                )
            }
        }
    }
}

@Composable
fun ProfileScreenButtonRow(
    modifier: Modifier,
    isEditing: Boolean = false,
    onBackClick: () -> Unit,
    onEditClick: () -> Unit,
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .heightIn(min = 56.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(
            onClick = onBackClick,
            modifier = Modifier
                .size(48.dp)
                .clip(MaterialTheme.shapes.small)
                .background(MaterialTheme.colors.primary)
        ) {
            // Back button
            Icon(
                imageVector = Icons.Default.ArrowBack,
                contentDescription = stringResource(R.string.back),
                tint = MaterialTheme.colors.onPrimary
            )
        }

        // Edit profile button
        if (!isEditing) {
            IconButton(
                onClick = onEditClick,
                modifier = Modifier
                    .size(48.dp)
                    .clip(MaterialTheme.shapes.small)
                    .background(MaterialTheme.colors.primary)
            ) {
                Icon(
                    imageVector = Icons.Default.Update,
                    contentDescription = stringResource(R.string.edit),
                    tint = MaterialTheme.colors.onPrimary
                )
            }
        }
    }
}

@Composable
private fun UserInfo(
    modifier: Modifier,
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    user: ProjectUser,
    isEditing: Boolean,
    onDoneClick: () -> Unit,

    isPreview: Boolean,
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        // Profile picture
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(240.dp)
                .clip(MaterialTheme.shapes.medium)
                .background(MaterialTheme.colors.primary)
        ) {
            if(isPreview) {
                Image(
                    modifier = modifier,
                    painter = fallbackPainterResource(R.drawable.restaurant_app_example_user_profile_picture_0),
                    contentDescription = stringResource(R.string.user_profile_image),
                    contentScale = ContentScale.Crop
                )
            } else {
                ImageFromFirebase(
                    firebaseImageRef = user.profilePictureFirebaseImageRef,
                    modifier = Modifier
                        .fillMaxSize()
                        .clip(MaterialTheme.shapes.medium),
                    contentDescription = stringResource(R.string.user_profile_image),
                    contentScale = ContentScale.Crop
                )
            }

            val changeProfilePicture: ()->Unit = {
                // Functionality unavailable in demo project.
            }

            // Edit profile picture button
            if (isEditing) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(
                            MaterialTheme.colors.background.copy(alpha = 0.8f)
                        )
                ) {
                    IconButton(
                        onClick = changeProfilePicture,
                        modifier = Modifier
                            .size(48.dp)
                            .clip(MaterialTheme.shapes.small)
                            .align(Alignment.BottomEnd)
                            .padding(16.dp)
                            .background(MaterialTheme.colors.primary)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Warning,
                            contentDescription = stringResource(R.string.edit_image),
                            tint = MaterialTheme.colors.onPrimary
                        )
                    }
                }
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        UserInfoItem(
            icon = Icons.Default.Person,
            label = stringResource(R.string.name),
            value = user.name ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.name = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

        Spacer(modifier = Modifier.height(16.dp))

        UserInfoItem(
            icon = Icons.Default.Person,
            label = stringResource(R.string.surname),
            value = user.surname ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.surname = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

        Spacer(modifier = Modifier.height(8.dp))

        UserInfoItem(
            icon = Icons.Default.MailOutline,
            label = stringResource(R.string.email),
            value = user.emailAddress ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.emailAddress = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

        Spacer(modifier = Modifier.height(8.dp))

        UserInfoItem(
            icon = Icons.Default.LocationCity,
            label = stringResource(R.string.city),
            value = user.city ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.city = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

        Spacer(modifier = Modifier.height(8.dp))

        UserInfoItem(
            icon = Icons.Default.Description,
            label = stringResource(R.string.about_me),
            value = user.description ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.description = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

        Spacer(modifier = Modifier.height(32.dp))

        if (isEditing) {
            Button(
                onClick = { onDoneClick() },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Check,
                    contentDescription = stringResource(R.string.done),
                    modifier = Modifier.size(24.dp),
                    tint = MaterialTheme.colors.onPrimary
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = stringResource(R.string.done),
                    style = MaterialTheme.typography.button,
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }

    }
}

@Composable
@Preview(showBackground = true)
private fun RestaurantAppOrderProfileScreenContentPreview() {
    val sampleUser = ProjectUser(
        id = UserUniqueID("123"),
        updated_at = "2023-01-01",
        created_at = "2023-01-01",
        emailAddress = "JoannaTracer@example.com",
        password = "securePassword",
        profilePhotoImageURI = null,
        name = "Joanna",
        surname = "Tracer",
        description = "A vivacious food enthusiast with an insatiable appetite for culinary adventures.",
        city = "London",
        emailAddressVerified = true,
        phoneNumberVerified = true
    )


    HomeScreenTheme {
        RestaurantAppOrderProfileScreenContent(
            user = sampleUser,
            onBackClick = {},
            isPreview = true,
        )
    }
}
