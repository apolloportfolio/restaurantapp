package com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "entities3")
data class ReservationFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("ownerID")
    @Expose
    var ownerID: UserUniqueID?,


    @SerializedName("date")
    @Expose
    var date: String?,

    @SerializedName("time")
    @Expose
    var time: String?,

    @SerializedName("numberOfGuests")
    @Expose
    var numberOfGuests: String?,
    ) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}