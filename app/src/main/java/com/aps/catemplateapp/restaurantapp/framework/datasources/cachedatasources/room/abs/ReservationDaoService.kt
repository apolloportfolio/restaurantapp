package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation

interface ReservationDaoService {
    suspend fun insertOrUpdateEntity(entity: Reservation): Reservation

    suspend fun insertEntity(entity: Reservation): Long

    suspend fun insertEntities(Entities: List<Reservation>): LongArray

    suspend fun getEntityById(id: UniqueID?): Reservation?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        date: String?,
        time: String?,
        numberOfGuests: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<Reservation>): Int

    suspend fun searchEntities(): List<Reservation>

    suspend fun getAllEntities(): List<Reservation>

    suspend fun getNumEntities(): Int
}