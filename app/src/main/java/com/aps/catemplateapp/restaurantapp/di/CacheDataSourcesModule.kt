package com.aps.catemplateapp.restaurantapp.di

import com.aps.catemplateapp.core.business.data.cache.abstraction.*
import com.aps.catemplateapp.core.business.data.cache.implementation.*
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.DishCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.OrderCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.ReservationCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.cache.implementation.DishCacheDataSourceImpl
import com.aps.catemplateapp.restaurantapp.business.data.cache.implementation.OrderCacheDataSourceImpl
import com.aps.catemplateapp.restaurantapp.business.data.cache.implementation.ReservationCacheDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheDataSourcesModule {

    @Binds
    abstract fun bindDishCacheDataSource(implementation: DishCacheDataSourceImpl): DishCacheDataSource

    @Binds
    abstract fun bindReservationCacheDataSource(implementation: ReservationCacheDataSourceImpl): ReservationCacheDataSource

    @Binds
    abstract fun bindOrderCacheDataSource(implementation: OrderCacheDataSourceImpl): OrderCacheDataSource
}