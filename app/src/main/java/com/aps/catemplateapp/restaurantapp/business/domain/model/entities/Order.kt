package com.aps.catemplateapp.restaurantapp.business.domain.model.entities

import android.os.Parcelable
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.IgnoredOnParcel
import java.io.Serializable

// Entity2

@Parcelize
data class Order(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,

    var picture1URI: String?,

    var description: String?,

    var ownerID: UserUniqueID?,

    var orderedDishesIds: String?,
): Parcelable, Serializable {

    @IgnoredOnParcel
    val picture1ImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_2_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture1URI!!)
            } else {
                null
            }
        }


    fun toIdString() : String {
        return "${this.javaClass.simpleName}($id)"
    }

    companion object {
        const val ownerIDFieldName = "ownerID"
    }
}