package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.OrderCacheEntity
import javax.inject.Inject

class OrderCacheMapper
@Inject
constructor() : EntityMapper<OrderCacheEntity, Order> {
    override fun mapFromEntity(entity: OrderCacheEntity): Order {
        return Order(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.ownerID,

            entity.orderedDishesIds,
        )
    }

    override fun mapToEntity(domainModel: Order): OrderCacheEntity {
        return OrderCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.description,

            domainModel.ownerID,

            domainModel.orderedDishesIds,
        )
    }

    override fun mapFromEntityList(entities : List<OrderCacheEntity>) : List<Order> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Order>): List<OrderCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}