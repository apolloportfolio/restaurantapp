package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.ReservationCacheEntity
import java.util.*


@Dao
interface ReservationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : ReservationCacheEntity) : Long

    @Query("SELECT * FROM reservation")
    suspend fun get() : List<ReservationCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<ReservationCacheEntity>): LongArray

    @Query("SELECT * FROM reservation WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): ReservationCacheEntity?

    @Query("DELETE FROM reservation WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM reservation")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM reservation")
    suspend fun getAllEntities(): List<ReservationCacheEntity>

    @Query("""
        UPDATE reservation 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        description = :description,
        ownerId = :ownerID,
        date = :date,
        time = :time,
        numberOfGuests = :numberOfGuests
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        date: String?,
        time: String?,
        numberOfGuests: String?,
    ): Int

    @Query("DELETE FROM reservation WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM reservation")
    suspend fun searchEntities(): List<ReservationCacheEntity>
    
    @Query("SELECT COUNT(*) FROM reservation")
    suspend fun getNumEntities(): Int
}