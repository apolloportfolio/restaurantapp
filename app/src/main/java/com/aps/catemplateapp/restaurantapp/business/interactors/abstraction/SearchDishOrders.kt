package com.aps.catemplateapp.restaurantapp.business.interactors.abstraction

import android.location.Location
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface SearchDishOrders {
    fun searchDishOrders(
        location : Location?,
        dishName: String,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<Order>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit = {},
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}