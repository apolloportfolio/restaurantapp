package com.aps.catemplateapp.restaurantapp.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.ReservationCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.ReservationDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReservationCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: ReservationDaoService
): ReservationCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: Reservation): Reservation? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: Reservation): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Reservation>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        date: String?,
        time: String?,
        numberOfGuests: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            description,

            ownerID,

            date,
            time,
            numberOfGuests,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Reservation> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<Reservation> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): Reservation? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<Reservation>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}