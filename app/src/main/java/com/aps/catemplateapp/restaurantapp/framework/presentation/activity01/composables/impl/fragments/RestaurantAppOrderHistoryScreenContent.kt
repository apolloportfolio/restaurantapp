package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType02
import com.aps.catemplateapp.common.framework.presentation.views.ProfileStatusBar
import com.aps.catemplateapp.common.framework.presentation.views.SearchBarWithClearButton
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.OrderFactory
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsRestaurantApp
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme

private const val TAG = "RestaurantAppOrderHistoryScreenContent"
private const val LOG_ME = true

@Composable
internal fun RestaurantAppOrderHistoryScreenContent(
    stateEventTracker: StateEventTracker,
    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    initialSearchQuery: String = "",
    entities: List<Order>?,
    onSearchQueryUpdate: (String) -> Unit,
    onListItemClick: (Order) -> Unit,
    launchInitStateEvent: () -> Unit,

    orderAllDishesAgain: (Order) -> Unit,
    showTitleBar: Boolean = true,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier,
            verticalArrangement = Arrangement.Center,
        ) {
            var searchQuery by remember { mutableStateOf(initialSearchQuery) }
            var searchQueryUpdated by remember { mutableStateOf(false) }

            val titleBackgroundColors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            )
            if(showTitleBar) {
                TitleRowType01(
                    titleString = stringResource(id = R.string.order_history),
                    textAlign = TextAlign.Center,
                    composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
                        colors = titleBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    leftPictureDrawableId = null,
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            }

            val searchBarBackgroundColors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            )

            SearchBarWithClearButton(
                searchQuery = searchQuery,
                onSearchQueryChange = { query -> searchQuery = query },
                onSearchBackClick = {
                    searchQuery = ""
                    searchQueryUpdated = true
                },
                onSearchClick = {
                    searchQueryUpdated = true
                },
                composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
                    colors = searchBarBackgroundColors,
                    brush = null,
                    shape = null,
                    alpha = 0.6f,
                ),
            )

            if(LOG_ME) ALog.d(
                TAG, "(): " +
                    "showProfileStatusBar = $showProfileStatusBar")
            if(showProfileStatusBar) {
                if(LOG_ME) ALog.d(
                    TAG, "(): " +
                        "Showing profile completion bar")

                val profileStatusBarBackgroundColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                )
                ProfileStatusBar(
                    finishVerification,
                    composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundScreen01(
                        colors = profileStatusBarBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    titleTint = MaterialTheme.colors.secondary,
                    subtitleTint = MaterialTheme.colors.error,
                )
            }

            if(entities?.isNotEmpty() == true) {
                LazyColumnWithPullToRefresh(
                    onRefresh = launchInitStateEvent,
                    isRefreshing = stateEventTracker.isRefreshing,
                ) {
                    itemsIndexed(entities) { index, item ->
                        ListItemType02(
                            index = index,
                            itemTitleString = item.created_at,
                            itemDescription = item.description,
                            onListItemClick = { onListItemClick(item) },
                            onItemsButtonClick = { orderAllDishesAgain(item) },
                            getItemsRating = { null },
                            itemRef = if(isPreview) {
                                null
                            } else {
                                item.picture1ImageRef
                            },
                            backgroundDrawableId = R.drawable.example_background_1,
                            composableBackground = BackgroundsOfLayoutsRestaurantApp.backgroundListItemType01(
                                colors = null,
                                brush = null,
                                shape = null,
                                alpha = 0.6f,
                            ),
                            itemDrawableId = if(isPreview) {
                                getDrawableIdInPreview("restaurant_app_example_dish_", index)
                            } else { null },
                            imageTint = null,
                            buttonTint = MaterialTheme.colors.secondary,
                        )
                    }
                }
            } else {
                TipContentIsUnavailable(stringResource(id = R.string.home_screen_fragment1_list_is_empty_tip))
            }

            var firstComposition by rememberSaveable { mutableStateOf(true) }
            LaunchedEffect(searchQueryUpdated) {
                if(firstComposition) {
                    if(LOG_ME) ALog.d(
                        TAG, "LaunchedEffect(searchFiltersUpdated): " +
                            "This is the first composition.")
                    firstComposition = false
                } else {
                    if(searchQueryUpdated) {
                        if(LOG_ME) ALog.d(TAG, "(): LaunchedEffect: searchQueryUpdated")
                        onSearchQueryUpdate(searchQuery)
                    }
                }
                searchQueryUpdated = false
            }

            if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
        }
    }
}

@Preview
@Composable
private fun RestaurantAppOrderHistoryScreenContentPreview1() {
    HomeScreenTheme {
        RestaurantAppOrderHistoryScreenContent(
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = true,
            finishVerification = {},
            initialSearchQuery = "",
            entities = OrderFactory.createPreviewEntitiesList(),
            onSearchQueryUpdate = {},
            onListItemClick = {},
            launchInitStateEvent = {},

            orderAllDishesAgain = {},
            showTitleBar = false,

            isPreview = true,
        )

    }
}

@Preview
@Composable
private fun RestaurantAppOrderHistoryScreenContentPreview2() {
    HomeScreenTheme {
        RestaurantAppOrderHistoryScreenContent(
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = false,
            finishVerification = {},
            initialSearchQuery = "",
            entities = OrderFactory.createPreviewEntitiesList(),
            onSearchQueryUpdate = {},
            onListItemClick = {},
            launchInitStateEvent = {},

            orderAllDishesAgain = {},
            showTitleBar = false,

            isPreview = true,
        )

    }
}

@Preview
@Composable
private fun RestaurantAppOrderHistoryScreenContentPreview3() {
    HomeScreenTheme {
        RestaurantAppOrderHistoryScreenContent(
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = true,
            finishVerification = {},
            initialSearchQuery = "",
            entities = OrderFactory.createPreviewEntitiesList(),
            onSearchQueryUpdate = {},
            onListItemClick = {},
            launchInitStateEvent = {},

            orderAllDishesAgain = {},
            showTitleBar = true,

            isPreview = true,
        )

    }
}

@Preview
@Composable
private fun RestaurantAppOrderHistoryScreenContentPreview4() {
    HomeScreenTheme {
        RestaurantAppOrderHistoryScreenContent(
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = false,
            finishVerification = {},
            initialSearchQuery = "",
            entities = OrderFactory.createPreviewEntitiesList(),
            onSearchQueryUpdate = {},
            onListItemClick = {},
            launchInitStateEvent = {},

            orderAllDishesAgain = {},
            showTitleBar = true,

            isPreview = true,
        )

    }
}