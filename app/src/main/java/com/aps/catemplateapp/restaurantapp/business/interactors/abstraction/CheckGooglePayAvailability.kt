package com.aps.catemplateapp.restaurantapp.business.interactors.abstraction

import android.content.Context
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenViewState
import com.google.android.gms.wallet.PaymentsClient
import kotlinx.coroutines.flow.Flow

interface CheckGooglePayAvailability {
    fun checkGooglePayAvailability(
        context: Context,
        continueFlag: Boolean?,
        stateEvent: StateEvent,
        onErrorAction: () -> Unit = {},
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (
            HomeScreenViewState<ProjectUser>,
            PaymentsClient?,
            Boolean?,
            Boolean?,
        )-> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}