package com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.mappers.ReservationFirestoreMapper
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.model.ReservationFirestoreEntity

interface ReservationFirestoreService: BasicFirestoreService<
        Reservation,
        ReservationFirestoreEntity,
        ReservationFirestoreMapper
        > {
    suspend fun getUsersEntities3(userId: UserUniqueID): List<Reservation>?

}