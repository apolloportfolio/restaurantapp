package com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.model.OrderFirestoreEntity
import javax.inject.Inject

class OrderFirestoreMapper
@Inject
constructor() : EntityMapper<OrderFirestoreEntity, Order> {
    override fun mapFromEntity(entity: OrderFirestoreEntity): Order {
        return Order(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.ownerID,

            entity.orderedDishesIds,
        )
    }

    override fun mapToEntity(domainModel: Order): OrderFirestoreEntity {
        return OrderFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.picture1URI,
            domainModel.description,
            domainModel.ownerID,

            domainModel.orderedDishesIds,
        )
    }

    override fun mapFromEntityList(entities : List<OrderFirestoreEntity>) : List<Order> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Order>): List<OrderFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}