package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.*
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.DishDaoService
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.DishDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.mappers.DishCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Entity1DaoServiceImpl"
private const val LOG_ME = true

@Singleton
class DishDaoServiceImpl
@Inject
constructor(
    private val dao: DishDao,
    private val mapper: DishCacheMapper,
    private val dateUtil: DateUtil
): DishDaoService {

    override suspend fun insertOrUpdateEntity(entity: Dish): Dish {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,

                entity.description,

                entity.city,

                entity.ownerID,

                entity.name,

                entity.price,
                entity.currency,
                entity.allergenInfo,
            )
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: Dish): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<Dish>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): Dish? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        price: Double?,
        currency: String?,
        allergenInfo: String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                description,

                city,

                ownerID,

                name,

                price,
                currency,
                allergenInfo,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,

                description,

                city,

                ownerID,

                name,

                price,
                currency,
                allergenInfo,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Dish>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<Dish> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<Dish> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}