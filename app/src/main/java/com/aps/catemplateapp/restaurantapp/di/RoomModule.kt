package com.aps.catemplateapp.restaurantapp.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.DishDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.OrderDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.ReservationDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideDishDao(database : ProjectRoomDatabase) : DishDao {
        return database.dishDao()
    }

    @Singleton
    @Provides
    fun provideOrderDao(database : ProjectRoomDatabase) : OrderDao {
        return database.orderDao()
    }

    @Singleton
    @Provides
    fun provideReservationDao(database : ProjectRoomDatabase) : ReservationDao {
        return database.reservationDao()
    }
}