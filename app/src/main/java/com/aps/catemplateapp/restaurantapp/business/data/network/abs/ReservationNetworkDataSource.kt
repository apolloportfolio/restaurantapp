package com.aps.catemplateapp.restaurantapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation

interface ReservationNetworkDataSource: StandardNetworkDataSource<Reservation> {
    override suspend fun insertOrUpdateEntity(entity: Reservation): Reservation?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: Reservation)

    override suspend fun insertDeletedEntities(Entities: List<Reservation>)

    override suspend fun deleteDeletedEntity(entity: Reservation)

    override suspend fun getDeletedEntities(): List<Reservation>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: Reservation): Reservation?

    override suspend fun getAllEntities(): List<Reservation>

    override suspend fun insertOrUpdateEntities(Entities: List<Reservation>): List<Reservation>?

    override suspend fun getEntityById(id: UniqueID): Reservation?

    suspend fun getUsersEntities3(userId: UserUniqueID): List<Reservation>?
}