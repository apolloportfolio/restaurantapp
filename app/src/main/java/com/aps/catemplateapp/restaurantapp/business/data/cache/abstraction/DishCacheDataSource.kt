package com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish

interface DishCacheDataSource: StandardCacheDataSource<Dish> {
    override suspend fun insertEntity(entity: Dish): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<Dish>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean? = null,
        switch2: Boolean? = null,
        switch3: Boolean? = null,
        switch4: Boolean? = null,
        switch5: Boolean? = null,
        switch6: Boolean? = null,
        switch7: Boolean? = null,

        price: Double?,
        currency: String?,
        allergenInfo: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Dish>

    override suspend fun getAllEntities(): List<Dish>

    override suspend fun getEntityById(id: UniqueID?): Dish?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<Dish>): LongArray
}