package com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.location.Location
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType03
import com.aps.catemplateapp.common.framework.presentation.views.ProfileStatusBar
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.business.domain.model.factories.ReservationFactory
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "HomeScreenCompFragment3"
private const val LOG_ME = true

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment3(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},
    projectUser: ProjectUser?,
    onReservationConfirmed: (Reservation) -> Unit,

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    entities: List<Reservation>?,
    onListItemClick: (Reservation) -> Unit,
    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},
    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: HomeScreenComposableFragment3BottomSheetActions,
) {
    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )

    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.w(TAG, "ShowContent(): " +
                    "activity != null, launching state event in view model.")
            launchStateEvent(
                HomeScreenStateEvent.GetEntities3
            )
        } else {
            if(LOG_ME) ALog.w(TAG, "ShowContent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = mutableSetOf(),
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = HomeScreenCompFragment3BottomSheet(
            scope = scope,
            sheetState = sheetState,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
        ),
        content = {
            RestaurantAppOrderReservationScreenContent(
                projectUser = projectUser,
                onReservationConfirmed = onReservationConfirmed,
            )
        }
    )
}

// Preview =========================================================================================
@Preview
@Composable
fun HomeScreenComposableFragment3Preview(
    @PreviewParameter(
        HomeScreenComposableFragment3ParamsProvider::class
    ) params: HomeScreenComposableFragment3Params
) {
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val bottomSheetActions = HomeScreenComposableFragment3BottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = true,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        val sampleUser = ProjectUser(
            id = UserUniqueID("123"),
            updated_at = "2023-01-01",
            created_at = "2023-01-01",
            emailAddress = "user@example.com",
            password = "securePassword",
            profilePhotoImageURI = null,
            name = "John",
            surname = "Doe",
            description = "A passionate developer",
            city = "Sample City",
            emailAddressVerified = true,
            phoneNumberVerified = true
        )

        HomeScreenCompFragment3(
            projectUser = sampleUser,
            onReservationConfirmed = {},
            stateEventTracker = StateEventTracker(),
            deviceLocation = params.deviceLocation,
            showProfileStatusBar = params.showProfileStatusBar,
            finishVerification = params.finishVerification,
            entities = params.entities,
            onListItemClick = params.onListItemClick,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,
            bottomSheetActions = bottomSheetActions,
        )
    }
}

class HomeScreenComposableFragment3ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment3Params> {
    override val values: Sequence<HomeScreenComposableFragment3Params> = sequenceOf(
        HomeScreenComposableFragment3Params(
            deviceLocation = DeviceLocation(
                locationPermissionGranted = true,
                location = Location("Lublin").apply {
                    latitude = 51.2465
                    longitude = 22.5684
                }
            ),
            showProfileStatusBar = true,
            finishVerification = {},
            entities = ReservationFactory.createPreviewEntitiesList(),
            onListItemClick = {},
            onSwipeLeft = {},
            onSwipeRight = {},
            onSwipeUp = {},
            onSwipeDown = {},
            floatingActionButtonDrawableId = R.drawable.ic_baseline_add_24,
            floatingActionButtonOnClick = { },
            floatingActionButtonContentDescription = "FAB 3",
        )
    )
}

data class HomeScreenComposableFragment3Params(
    val deviceLocation: DeviceLocation?,
    val showProfileStatusBar: Boolean,
    val finishVerification: () -> Unit,
    val entities: List<Reservation>?,
    val onListItemClick: (Reservation) -> Unit,
    val onSwipeLeft: () -> Unit,
    val onSwipeRight: () -> Unit,
    val onSwipeUp: () -> Unit,
    val onSwipeDown: () -> Unit,
    val floatingActionButtonDrawableId: Int?,
    val floatingActionButtonOnClick: (() -> Unit)?,
    val floatingActionButtonContentDescription: String?,
)