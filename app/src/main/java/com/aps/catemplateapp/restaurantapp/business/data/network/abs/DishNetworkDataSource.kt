package com.aps.catemplateapp.restaurantapp.business.data.network.abs

import android.net.Uri
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.business.interactors.implementation.FirestoreEntity1SearchParameters

interface DishNetworkDataSource: StandardNetworkDataSource<Dish> {
    override suspend fun insertOrUpdateEntity(entity: Dish): Dish?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: Dish)

    override suspend fun insertDeletedEntities(Entities: List<Dish>)

    override suspend fun deleteDeletedEntity(entity: Dish)

    override suspend fun getDeletedEntities(): List<Dish>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: Dish): Dish?

    override suspend fun getAllEntities(): List<Dish>

    override suspend fun insertOrUpdateEntities(Entities: List<Dish>): List<Dish>?

    override suspend fun getEntityById(id: UniqueID): Dish?

    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<Dish>?

    suspend fun getUsersEntites1(userID: UserUniqueID) : List<Dish>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : Dish,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}