package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.DishCacheEntity
import javax.inject.Inject

class DishCacheMapper
@Inject
constructor() : EntityMapper<DishCacheEntity, Dish> {
    override fun mapFromEntity(entity: DishCacheEntity): Dish {
        return Dish(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,

            entity.price,
            entity.currency,
            entity.allergenInfo,
        )
    }

    override fun mapToEntity(domainModel: Dish): DishCacheEntity {
        return DishCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,

            domainModel.city,

            domainModel.ownerID,

            domainModel.name,

            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,

            domainModel.price,
            domainModel.currency,
            domainModel.allergenInfo,
        )
    }

    override fun mapFromEntityList(entities : List<DishCacheEntity>) : List<Dish> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Dish>): List<DishCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}