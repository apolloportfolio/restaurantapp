package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.DishCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface DishDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : DishCacheEntity) : Long

    @Query("SELECT * FROM dish")
    suspend fun get() : List<DishCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<DishCacheEntity>): LongArray

    @Query("SELECT * FROM dish WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): DishCacheEntity?

    @Query("DELETE FROM dish WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM dish")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM dish")
    suspend fun getAllEntities(): List<DishCacheEntity>

    @Query(
        """
        UPDATE dish 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        description = :description,
        city = :city,
        ownerID = :ownerID,
        name = :name,
        price = :price,
        currency = :currency,
        allergenInfo = :allergenInfo
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,

        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        price: Double?,
        currency: String?,
        allergenInfo: String?,
    ): Int

    @Query("DELETE FROM dish WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM dish")
    suspend fun searchEntities(): List<DishCacheEntity>
    
    @Query("SELECT COUNT(*) FROM dish")
    suspend fun getNumEntities(): Int
}