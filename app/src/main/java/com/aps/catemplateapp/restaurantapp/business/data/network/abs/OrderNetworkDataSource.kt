package com.aps.catemplateapp.restaurantapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order

interface OrderNetworkDataSource: StandardNetworkDataSource<Order> {
    override suspend fun insertOrUpdateEntity(entity: Order): Order?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: Order)

    override suspend fun insertDeletedEntities(Entities: List<Order>)

    override suspend fun deleteDeletedEntity(entity: Order)

    override suspend fun getDeletedEntities(): List<Order>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: Order): Order?

    override suspend fun getAllEntities(): List<Order>

    override suspend fun insertOrUpdateEntities(Entities: List<Order>): List<Order>?

    override suspend fun getEntityById(id: UniqueID): Order?

    suspend fun searchEntities(
        dishName: String,
    ) : List<Order>?

    suspend fun getUsersEntities2(userId : UserUniqueID) : List<Order>?
}