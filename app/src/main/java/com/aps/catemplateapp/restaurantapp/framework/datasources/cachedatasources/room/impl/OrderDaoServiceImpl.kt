package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.OrderDaoService
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.daos.OrderDao
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.mappers.OrderCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderDaoServiceImpl
@Inject
constructor(
    private val dao: OrderDao,
    private val mapper: OrderCacheMapper,
    private val dateUtil: DateUtil
): OrderDaoService {

    override suspend fun insertOrUpdateEntity(entity: Order): Order {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.picture1URI,

                entity.description,

                entity.ownerID,

                entity.orderedDishesIds,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: Order): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<Order>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): Order? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        orderedDishesIds: String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                picture1URI,

                description,

                ownerID,

                orderedDishesIds,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                picture1URI,

                description,

                ownerID,

                orderedDishesIds,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Order>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<Order> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<Order> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}