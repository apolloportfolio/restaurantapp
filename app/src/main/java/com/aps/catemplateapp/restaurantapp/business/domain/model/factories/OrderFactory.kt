package com.aps.catemplateapp.restaurantapp.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.random.Random

@Singleton
class OrderFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        picture1URI: String?,
        description : String?,

        ownerID: UserUniqueID?,

        orderedDishesIds: String?,
    ): Order {
        return Order(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            picture1URI,
            description,
            ownerID,

            orderedDishesIds,
        )
    }

    fun createEntitiesList(numEntities: Int): List<Order> {
        val list: ArrayList<Order> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): Order {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<Order> {
            val dishes = listOf(
                "Spaghetti Carbonara",
                "Chicken Tikka Masala",
                "Margherita Pizza",
                "Caesar Salad",
                "Sushi Sashimi Combo",
                "Beef Burgundy",
                "Pad Thai",
                "Vegetable Stir-Fry",
                "Grilled Salmon",
                "Cheeseburger"
            )
            return arrayListOf(
                Order(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                ),
                Order(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = dishes.shuffled().take(Random.nextInt(1, 4)).joinToString("\n"),
                    ownerID = UserUniqueID("666", "666"),
                    orderedDishesIds = null,
                )
            )
        }
    }
}