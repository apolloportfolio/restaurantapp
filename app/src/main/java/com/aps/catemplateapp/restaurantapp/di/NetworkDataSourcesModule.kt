package com.aps.catemplateapp.restaurantapp.di

import com.aps.catemplateapp.restaurantapp.business.data.network.abs.DishNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.OrderNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.ReservationNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.implementation.DishNetworkDataSourceImpl
import com.aps.catemplateapp.restaurantapp.business.data.network.implementation.OrderNetworkDataSourceImpl
import com.aps.catemplateapp.restaurantapp.business.data.network.implementation.ReservationNetworkDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkDataSourcesModule {

    @Binds
    abstract fun bindDishNetworkDataSource(implementation: DishNetworkDataSourceImpl): DishNetworkDataSource

    @Binds
    abstract fun bindOrderNetworkDataSource(implementation: OrderNetworkDataSourceImpl): OrderNetworkDataSource

    @Binds
    abstract fun bindReservationNetworkDataSource(implementation: ReservationNetworkDataSourceImpl): ReservationNetworkDataSource

}