package com.aps.catemplateapp.restaurantapp.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.implementation.*
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.DishDaoService
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.OrderDaoService
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.abs.ReservationDaoService
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.impl.DishDaoServiceImpl
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.impl.OrderDaoServiceImpl
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.impl.ReservationDaoServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RoomDAOsModule {

    @Binds
    abstract fun bindDishDaoService(implementation: DishDaoServiceImpl): DishDaoService

    @Binds
    abstract fun bindOrderDaoService(implementation: OrderDaoServiceImpl): OrderDaoService

    @Binds
    abstract fun bindReservationDaoService(implementation: ReservationDaoServiceImpl): ReservationDaoService

}