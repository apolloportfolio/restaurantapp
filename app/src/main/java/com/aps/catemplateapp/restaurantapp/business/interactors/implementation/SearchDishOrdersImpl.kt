package com.aps.catemplateapp.restaurantapp.business.interactors.implementation

import android.location.Location
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.MessageType
import com.aps.catemplateapp.common.business.domain.state.Response
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.business.domain.state.UIComponentType
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction.OrderCacheDataSource
import com.aps.catemplateapp.restaurantapp.business.data.network.abs.OrderNetworkDataSource
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Order
import com.aps.catemplateapp.restaurantapp.business.interactors.abstraction.SearchDishOrders
import com.aps.catemplateapp.restaurantapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "SearchDishOrdersImpl"
private const val LOG_ME = true

class SearchDishOrdersImpl
@Inject
constructor(
    private val orderCacheDataSource: OrderCacheDataSource,
    private val orderNetworkDataSource: OrderNetworkDataSource,
): SearchDishOrders {
    override fun searchDishOrders(
        location : Location?,
        dishName: String,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<Order>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val syncedEntities = safeCacheCall(Dispatchers.IO, onErrorAction = onErrorAction) {
            if (LOG_ME) ALog.d(TAG, ".searchDishOrders(): dishName: \n$dishName")
            syncEntities(location, dishName)
        }


        val response = object : CacheResponseHandler<HomeScreenViewState<ProjectUser>, List<Order>>(
            response = syncedEntities,
            stateEvent = stateEvent
        ) {
            override suspend fun handleSuccess(resultObj: List<Order>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GET_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if (LOG_ME) ALog.d(TAG, "searchDishOrders().handleSuccess(): ")


                if (resultObj == null) {
                    if (LOG_ME) ALog.d(TAG, "searchDishOrders(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    if (LOG_ME) ALog.d(TAG, "searchDishOrders(): resultObj != null")
                    returnViewState.dishOrdersList = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    // Method downloads entities from network and saves them in cache
    private suspend fun syncEntities(
        location : Location?,
        dishName: String,
    ) : List<Order> {
        val networkEntitiesList = getNetworkEntities(dishName)
        return networkEntitiesList
    }

    private suspend fun getNetworkEntities(dishName: String): List<Order>{
        val networkResult = safeApiCall(Dispatchers.IO) {
            if (LOG_ME) ALog.d(TAG, ".getNetworkEntities(): dishName: \n$dishName")
            orderNetworkDataSource.searchEntities(dishName)
        }

        val response = object: ApiResponseHandler<List<Order>, List<Order>>(
            response = networkResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<Order>): DataState<List<Order>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    companion object {
        const val GET_ENTITIES_SUCCESS = "Successfully searched entities."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."
    }
}