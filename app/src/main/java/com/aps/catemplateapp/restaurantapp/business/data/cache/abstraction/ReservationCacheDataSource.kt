package com.aps.catemplateapp.restaurantapp.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation

interface ReservationCacheDataSource: StandardCacheDataSource<Reservation> {
    override suspend fun insertOrUpdateEntity(entity: Reservation): Reservation?

    override suspend fun insertEntity(entity: Reservation): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<Reservation>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        date: String?,
        time: String?,
        numberOfGuests: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Reservation>

    override suspend fun getAllEntities(): List<Reservation>

    override suspend fun getEntityById(id: UniqueID?): Reservation?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<Reservation>): LongArray
}