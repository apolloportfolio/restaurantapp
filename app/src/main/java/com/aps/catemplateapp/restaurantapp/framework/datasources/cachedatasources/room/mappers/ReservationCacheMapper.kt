package com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Reservation
import com.aps.catemplateapp.restaurantapp.framework.datasources.cachedatasources.room.model.ReservationCacheEntity
import javax.inject.Inject

class ReservationCacheMapper
@Inject
constructor() : EntityMapper<ReservationCacheEntity, Reservation> {
    override fun mapFromEntity(entity: ReservationCacheEntity): Reservation {
        return Reservation(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.ownerID,

            entity.date,
            entity.time,
            entity.numberOfGuests,
        )
    }

    override fun mapToEntity(domainModel: Reservation): ReservationCacheEntity {
        return ReservationCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.description,

            domainModel.ownerID,

            domainModel.date,
            domainModel.time,
            domainModel.numberOfGuests,
        )
    }

    override fun mapFromEntityList(entities : List<ReservationCacheEntity>) : List<Reservation> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Reservation>): List<ReservationCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}