package com.aps.catemplateapp.restaurantapp.di

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs.DishFirestoreService
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs.OrderFirestoreService
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.abs.ReservationFirestoreService
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.impl.DishFirestoreServiceImpl
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.impl.OrderFirestoreServiceImpl
import com.aps.catemplateapp.restaurantapp.framework.datasources.networkdatasources.firebase.impl.ReservationFirestoreServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindDishFirestoreService(implementation: DishFirestoreServiceImpl): DishFirestoreService

    @Binds
    abstract fun bindOrderFirestoreService(implementation: OrderFirestoreServiceImpl): OrderFirestoreService

    @Binds
    abstract fun bindReservationFirestoreService(implementation: ReservationFirestoreServiceImpl): ReservationFirestoreService



}