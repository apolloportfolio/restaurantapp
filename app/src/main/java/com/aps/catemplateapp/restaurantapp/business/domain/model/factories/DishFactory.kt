package com.aps.catemplateapp.restaurantapp.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.restaurantapp.business.domain.model.entities.Dish
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DishFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,
        price: Double?,
        currency: String?,
        allergenInfo: String?,
    ): Dish {
        return Dish(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,

            city,

            ownerID,

            name,
            
            null,
            null,
            null,
            null,
            null,
            null,
            null,

            price,
            currency,
            allergenInfo,
        )
    }

    fun createEntitiesList(numEntities: Int): List<Dish> {
        val list: ArrayList<Dish> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): Dish {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }
    companion object {
        fun createPreviewEntitiesList(): List<Dish> {
            return arrayListOf(
                Dish(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Indulge in the opulence of the ocean with our exquisite Royal Symphony of the Sea, a gastronomic masterpiece fit for royalty. Begin your culinary voyage with a delicate tower of freshly caught Alaskan king crab legs, meticulously arranged around a center of chilled lobster medallions, drizzled with a velvety saffron-infused aioli.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Royal Symphony of the Sea",
                    price = 39.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Embark on a journey through the mystique of twilight with our enchanting Twilight Elixir Infusion, a culinary marvel that intertwines flavors of the night to create an unforgettable dining experience.\n" +
                            "\n" +
                            "Begin your voyage with a delicate amuse-bouche of ruby-red beetroot carpaccio, adorned with crumbles of creamy goat cheese and sprinkled with toasted hazelnuts, evoking the hues of a dusky sunset. The earthy sweetness of the beetroot harmonizes with the tanginess of the goat cheese, while the nutty crunch of hazelnuts adds a delightful contrast of texture.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Twilight Elixir Infusion",
                    price = 29.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "test03",
                    price = 19.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "test04",
                    price = 69.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "test05",
                    price = 49.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "test06",
                    price = 39.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "test07",
                    price = 39.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "test08",
                    price = 39.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "test09",
                    price = 39.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                ),
                Dish(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Embark on a celestial journey through the bountiful flavors of the harvest with our exquisite Celestial Harvest Symphony, a culinary opus that celebrates the abundance of the earth in a symphony of taste and texture.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Celestial Harvest Symphony",
                    price = 39.00,
                    currency = "$",
                    allergenInfo = "May contain nuts and bolts or milk."
                )
            )
        }
    }
}