package com.aps.catemplateapp.common.util.extensions

fun Collection<*>?.getSizeString(): String {
    return this?.size.toString() ?: "null"
}
