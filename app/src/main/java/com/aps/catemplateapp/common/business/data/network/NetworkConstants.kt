package com.aps.catemplateapp.common.business.data.network

object NetworkConstants {

    const val NETWORK_TIMEOUT = 30000L
}