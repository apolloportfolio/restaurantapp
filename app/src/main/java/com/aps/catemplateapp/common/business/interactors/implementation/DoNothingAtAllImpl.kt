package com.aps.catemplateapp.common.business.interactors.implementation

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class DoNothingAtAllImpl<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>>
@Inject constructor(): DoNothingAtAll<
        Entity,
        CacheDataSource,
        NetworkDataSource,
        ViewState> {

    override fun doNothing(): Flow<DataState<ViewState>?> = flow {
       //Nothing to do
    }
}
