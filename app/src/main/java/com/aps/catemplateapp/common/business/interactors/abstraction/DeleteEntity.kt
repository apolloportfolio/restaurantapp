package com.aps.catemplateapp.common.business.interactors.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.util.UniqueID
import kotlinx.coroutines.flow.Flow

interface DeleteEntity<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>> {
    fun deleteEntity(
        entity: Entity,
        stateEvent: StateEvent,
        getId: (entity: Entity) -> UniqueID?
    ): Flow<DataState<ViewState>?>
}