package com.aps.catemplateapp.common.util.extensions

import android.content.Context
import android.net.ConnectivityManager
import androidx.test.core.app.ApplicationProvider

fun internetConnectionIsAvailable(context : Context = (ApplicationProvider.getApplicationContext() as Context)) : Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}