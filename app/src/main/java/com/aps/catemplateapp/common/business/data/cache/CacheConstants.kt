package com.aps.catemplateapp.common.business.data.cache

object CacheConstants {

    const val CACHE_TIMEOUT = 4000L
}