package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import android.content.Context
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.LoginUser
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class LoginUserImpl
@Inject
constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val entityFactory: UserFactory
): LoginUser {

    override fun loginUser(
        newEntity: ProjectUser,
        stateEvent: StateEvent,
        okButtonCallbackWhenNoInternetConnection: OkButtonCallback,
    ):Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val registerResult = safeApiCall(Dispatchers.IO){
            networkDataSource.loginUser(newEntity)
        }

        val apiResponse = object: ApiResponseHandler<RegisterLoginActivityViewState<ProjectUser>, FirebaseUser?>(
            response = registerResult,
            stateEvent = stateEvent,
            networkErrorMessage = (getApplicationContext() as Context).getString(
                R.string.NO_INTERNET_CONNECTION_ENABLE_AND_RESTART
            ),
            networkDataNullMessage = (getApplicationContext() as Context).getString(
                R.string.unsuccessful_login
            ),
            okButtonCallbackWhenNoInternetConnection = okButtonCallbackWhenNoInternetConnection,
        ){
            override suspend fun handleSuccess(resultObj: FirebaseUser?): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                return if(resultObj != null){
                    val viewState =
                        RegisterLoginActivityViewState<ProjectUser>(
                            firebaseUser = resultObj,
                            firebaseUserIsLoggedIn = true
                        )
                    DataState.data(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = LOGIN_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                }
                else{
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = LOGIN_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(apiResponse)
    }

    companion object{
        val LOGIN_SUCCESS = "Login successful."
        val LOGIN_FAILED = "Login failed."
    }
}