package com.aps.catemplateapp.feature01.framework.presentation.activity01.state

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.business.domain.state.StateMessage
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser

sealed class RegisterLoginActivityStateEvent: StateEvent {

    object CheckIfEmailIsTakenUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_check_if_email_is_taken_error)
        }

        override fun eventName(): String {
            return "CheckIfEmailIsTakenUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }


    object CheckIfPasswordMatchesEmailUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_check_if_password_matches_error)
        }

        override fun eventName(): String {
            return "CheckIfPasswordMatchesEmailUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object RegisterUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_register_user_error)
        }

        override fun eventName(): String {
            return "RegisterUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }


    object LoginUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_login_user_error)
        }

        override fun eventName(): String {
            return "LoginUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object GetAppUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_get_appuser_error)
        }

        override fun eventName(): String {
            return "GetAppUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object AddNewAppUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_add_new_appuser_error)
        }

        override fun eventName(): String {
            return "AddNewAppUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object InsertNewUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_insert_new_user_error)
        }

        override fun eventName(): String {
            return "InsertNewUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object InsertMultipleNewUsersEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_insert_multiple_new_users_error)
        }

        override fun eventName(): String {
            return "InsertMultipleNewUsersEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object GetAllCacheEntities: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_get_all_cache_entities_error)
        }

        override fun eventName(): String {
            return "GetAllCacheEntities"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object SearchUserEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_search_appuser_error)
        }

        override fun eventName(): String {
            return "SearchUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object LoginAppUserStateEvent: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_login_appuser_error)
        }

        override fun eventName(): String {
            return "LoginAppUser"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object CheckDeviceCompatibility: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_check_device_compatibility_error)
        }

        override fun eventName(): String {
            return "CheckDeviceCompatibility"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class UpdateAppUserEvent(
        val appProjectUser: ProjectUser
    ): RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_update_appuser_error)
        }

        override fun eventName(): String {
            return "UpdateAppUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class DeleteAppUserEvent(
        val appProjectUser: ProjectUser
    ): RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_delete_appuser_error)
        }

        override fun eventName(): String {
            return "DeleteAppUserEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class DeleteMultipleAppUsersEvent(
        val appProjectUsers: ArrayList<ProjectUser>
    ): RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_delete_multiple_users_error)
        }

        override fun eventName(): String {
            return "DeleteMultipleAppUsersEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class CreateStateMessageEvent(
        val stateMessage: StateMessage
    ): RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_create_state_message_error)
        }

        override fun eventName(): String {
            return "CreateStateMessageEvent"
        }

        override fun shouldDisplayProgressBar() = false
    }

    object None: RegisterLoginActivityStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.start_state_event_none_error)
        }

        override fun eventName(): String {
            return "None"
        }

        override fun shouldDisplayProgressBar() = false
    }
}
