package com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction

interface SyncUsers {
    suspend fun syncEntities()
}