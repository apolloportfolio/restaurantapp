package com.aps.catemplateapp.feature01.di.activity01

import com.aps.catemplateapp.common.business.interactors.abstraction.*
import com.aps.catemplateapp.common.business.interactors.implementation.*
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.*
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation.*
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object RegisterLoginActivityInteractorsModule {
    @Provides
    fun provideRegisterUser(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        entityFactory: UserFactory
    ): RegisterUser {
        return RegisterUserImpl(cacheDataSource, networkDataSource, entityFactory)
    }
    @Provides
    fun provideDeleteMultipleUsers(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DeleteMultipleUsers {
        return DeleteMultipleUsersImpl(cacheDataSource, networkDataSource)
    }
    @Provides
    fun provideDeleteUser(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DeleteUser<RegisterLoginActivityViewState<ProjectUser>> {
        return DeleteUserImpl<RegisterLoginActivityViewState<ProjectUser>>(cacheDataSource, networkDataSource)
    }
    @Provides
    fun provideGetAllCachedUsers(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        entityFactory: UserFactory
    ): GetAllCachedUsers {
        return GetAllCachedUsersImpl(cacheDataSource)
    }
    @Provides
    fun provideGetUser(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        dateUtil: DateUtil
    ): GetUser {
        return GetUserImpl(cacheDataSource, networkDataSource, dateUtil)
    }
    @Provides
    fun provideInsertMultipleUsers(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): InsertMultipleUsers {
        return InsertMultipleUsersImpl(cacheDataSource, networkDataSource)
    }
    @Provides
    fun provideInsertUser(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        entityFactory: UserFactory
    ): InsertUser {
        return InsertUserImpl(cacheDataSource, networkDataSource, entityFactory)
    }
    @Provides
    fun provideLoginUser(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        entityFactory: UserFactory
    ): LoginUser {
        return LoginUserImpl(cacheDataSource, networkDataSource, entityFactory)
    }
    @Provides
    fun provideSearchCachedUsers(
        cacheDataSource: UserCacheDataSource
    ): SearchCachedUsers {
        return SearchCachedUsersImpl(cacheDataSource)
    }
    @Provides
    fun provideSyncUsers(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        dateUtil: DateUtil
    ): SyncUsers {
        return SyncUsersImpl(cacheDataSource, networkDataSource, dateUtil)
    }
    @Provides
    fun provideUpdateUser(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): UpdateUser {
        return UpdateUserImpl(cacheDataSource, networkDataSource)
    }
    @Provides
    fun provideCheckIfEmailIsTaken(
        networkDataSource: UserNetworkDataSource
    ): CheckIfEmailIsTaken {
        return CheckIfEmailIsTakenImpl(networkDataSource)
    }
    @Provides
    fun provideCheckIfPasswordMatchesEmail(
        networkDataSource: UserNetworkDataSource
    ): CheckIfPasswordMatchesEmail {
        return CheckIfPasswordMatchesEmailImpl(networkDataSource)
    }


    @Provides
    fun provideDeleteEntity(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        entityFactory: UserFactory
    ): DeleteEntity<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>> {
        return DeleteEntityImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>>(cacheDataSource, networkDataSource)
    }

    @Provides
    fun provideGetAllCachedEntities(
        cacheDataSource: UserCacheDataSourceImpl
    ): GetAllCachedEntities<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>> {
        return GetAllCachedEntitiesImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>>(cacheDataSource)
    }

    @Provides
    fun provideGetEntity(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        dateUtil: DateUtil
    ): GetEntity<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>> {
        return GetEntityImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>>(cacheDataSource, networkDataSource, dateUtil)
    }

    @Provides
    fun provideInsertEntity(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        entityFactory: UserFactory
    ): InsertEntity<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>> {
        return InsertEntityImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>>(cacheDataSource, networkDataSource, )
    }

    @Provides
    fun provideUpdateEntity(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource,
        entityFactory: UserFactory
    ): UpdateEntity<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>> {
        return UpdateEntityImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>>(cacheDataSource, networkDataSource)
    }

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>> {
        return DoNothingAtAllImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>>()
    }
}