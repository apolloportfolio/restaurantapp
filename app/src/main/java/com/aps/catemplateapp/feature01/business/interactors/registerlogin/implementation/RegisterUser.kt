package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import android.content.Context
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.RegisterUser
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RegisterUserImpl
@Inject
constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val entityFactory: UserFactory
): RegisterUser {


    override fun registerUser(
        newEntity: ProjectUser,
        stateEvent: StateEvent
    ):Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val registerResult = safeApiCall(Dispatchers.IO){
            networkDataSource.registerNewUser(newEntity)
        }

        val cacheResponse = object: ApiResponseHandler<RegisterLoginActivityViewState<ProjectUser>, FirebaseUser?>(
            response = registerResult,
            stateEvent = stateEvent,
            networkDataNullMessage = (getApplicationContext() as Context).getString(R.string.possible_registration_problems)
        ){
            override suspend fun handleSuccess(resultObj: FirebaseUser?): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                if(resultObj != null){
                    val viewState =
                        RegisterLoginActivityViewState<ProjectUser>(
                            firebaseUser = resultObj,
                            firebaseUserIsRegistered = true
                        )
                    return DataState.data(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = REGISTER_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                } else {
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = REGISTER_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(cacheResponse)
    }

    companion object{
        val REGISTER_SUCCESS = "Successfully registered new user."
        val REGISTER_FAILED = "Failed to register new user."
    }
}