package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation


import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.DoNothing
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class DoNothingImpl @Inject constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource
): DoNothing {

    override fun doNothing(): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
       //Nothing to do
    }
}
