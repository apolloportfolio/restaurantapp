/** Adding Firebase functions:
 https://firebase.google.com/docs/functions/get-started?gen=2nd
 https://www.youtube.com/watch?v=dLB7s05epkY&t=120s
 In Android Studio's Terminal:
 npm install -g firebase-tools
 Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process
 firebase login
 firebase init firestore
 firebase init functions
 */

/** Deploying Firebase functions
Implement functions in app/functions.index.js then run in Android Terminal:
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process
cd functions
npm run lint -- --fix
firebase deploy --only functions
 */

// Solution to most formatting problems:
// npm run lint -- --fix

// Solution for "Expected linebreaks to be 'LF' but found 'CRLF'":
// https://stackoverflow.com/questions/4459011/change-eol-character-in-visual-studio

/**
 * Import function triggers from their respective submodules:
 *
 * const {onCall} = require("firebase-functions/v2/https");
 * const {onDocumentWritten} = require("firebase-functions/v2/firestore");
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */
// The Cloud Functions for Firebase SDK to create Cloud Functions and triggers.
// const {logger} = require("firebase-functions");
// const {onRequest} = require("firebase-functions/v2/https");
// const {onDocumentCreated} = require("firebase-functions/v2/firestore");

// The Firebase Admin SDK to access Firestore.
// const {getFirestore} = require("firebase-admin/firestore");

// Create and deploy your first functions
// https://firebase.google.com/docs/functions/get-started

// exports.helloWorld = onRequest((request, response) => {
//   logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });


const functions = require("firebase-functions");
const {initializeApp} = require("firebase-admin/app");
initializeApp();

exports.getEmailAddressForEmailVerification =
    functions.https.onCall((data, context) => {
      return "exampleEmailAddress";
    });

exports.getEmailPasswordForEmailVerification =
    functions.https.onCall((data, context) => {
      return "examplePassword";
    });

exports.getGatewayNameForProject =
    functions.https.onCall((data, context) => {
      return "exampleGatewayName";
    });

exports.getGatewayMerchantID =
    functions.https.onCall((data, context) => {
      return "exampleGatewayMerchantId";
    });

exports.getMerchantName =
    functions.https.onCall((data, context) => {
      return "exampleMerchantName";
    });

exports.getDirectTokenizationPublicKey =
    functions.https.onCall((data, context) => {
      return "exampleDirectTokenizationPublicKey";
    });
